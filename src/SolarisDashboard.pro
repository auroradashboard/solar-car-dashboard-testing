TEMPLATE = subdirs

CONFIG += debug_and_release

SUBDIRS = \
    DashboardLibrary \
    TestDataBackend \
    TestDataSource \
    Frontend \
    TestFrontend

TestDataBackend.depends += DashboardLibrary
TestDataSource.depends += DashboardLibrary
TestDataFrontend.depends += DashboardLibrary
Frontend.depends += DashboardLibrary
