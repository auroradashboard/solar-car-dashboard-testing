/******************************************************************************\
|  Includes                                                                    |
\******************************************************************************/

#include <atomic>
#include <csignal>
#include <iostream>
#include <thread>

#include <dashboardlibrary.h>

/******************************************************************************\
|  Constants                                                                   |
\******************************************************************************/

const char* default_listen_address = "0.0.0.0";
const char* default_multicast_address = "239.255.60.60";
const char* default_udp_port = "4876";

const char* default_tcp_port = "4879";

const char* db_path = "test.sqlite";

/******************************************************************************\
| Global Variables                                                             |
\******************************************************************************/

// Port number to use for StreamServers/StreamClients
unsigned int next_port_number = atoi(default_tcp_port) + 1;

std::atomic<bool>* running;

/******************************************************************************\
|  Functions                                                                   |
\******************************************************************************/

using namespace std;
using namespace Solaris;

/**
 * @brief sigint_handler sets the global variable running to false
 * @param signal
 *
 * This should be attached to the signal using std::signal()
 */
void sigint_handler(int signal)
{
    running->store(false);
    std::signal(SIGINT, sigint_handler);
}

/**
 * @brief Runs the provided io_service and blocks until it returns.
 * @param io_service The io_service to be run.
 *
 * This function is intended to be launced in a separate thread to allow the
 * io_service to handle asynchronous network comms while other processing 
 * occurs in the main thread.
 
 * This thread will sometimes terminate on its own (when io_service.run()
 * returns) but can also be stopped by calling io_service.stop() from another
 * thread.
 */
void run_io_service(boost::asio::io_service& io_service)
{
    io_service.run();
}

int main()
{
    // Setup

    running = new std::atomic<bool>;
    running->store(true);

    // Connect the SIGINT signal to our handler to set running = false
    // Lets the program be killed using Ctrl+C
    std::signal(SIGINT, sigint_handler);

    CAN::Network *network = new CAN::Network(CAR_CONFIG_FILE);
    Buffer::NetworkBuffer *networkBuffer = new Buffer::NetworkBuffer(network);
    DB::Database *db = new DB::Database(db_path, networkBuffer);

	// TODO Make this an option
    db->drop_tables();
    
    db->create_tables();

    std::cout << "Initialising server" << std::endl;
    std::cout << "Hardware concurrency: " << std::thread::hardware_concurrency() << std::endl;

    boost::asio::io_service my_io_service;
    boost::asio::io_service::work my_work(my_io_service);

    TCP::Server *server = new TCP::Server(my_io_service, network, db, running, default_tcp_port);
    UDP::Receiver *receiver = new UDP::Receiver(my_io_service,
                                                running,
                                                default_listen_address,
                                                default_multicast_address,
                                                default_udp_port);

	// Start the UDP::Receiver and TCP::Server listening
    server->start_accept();
    receiver->start_receive();

	// Start the io_service to begin async network comms
    std::thread io_service_thread(run_io_service, std::ref(my_io_service));

    std::cout << "Server running..." << std::endl;

    while(running->load())
    {
        // Wait until a CAN::Frame is available
        UDP::CAN_Frame* cf = receiver->get_can_frame();
        if(cf != nullptr)
        {
            Buffer::ChannelGroupValue *cgv = networkBuffer->process_can_frame(cf);

            if(cgv != nullptr)
            {
                server->run_stream_servers(cgv);

                networkBuffer->buffer(cgv);
                db->check_buffers();
                delete cf;
            }
        }
    }

	// Stop listening and then network comms
    receiver->stop_receive();
    server->stop_accept();
    my_io_service.stop();

	// Make sure the io_service thread has terminated
    io_service_thread.join();

    std::cout << std::endl;
    std::cout << "All network comms ended" << std::endl;

    // Cleanup

    std::cout << "Flushing buffers to database..." << std::endl;
    db->clear_buffers();

    std::cout << "Quitting..." << std::endl;

    delete receiver;
    delete server;

    delete db;
    delete networkBuffer;
    delete network;

    delete running;

    return 0;
}
