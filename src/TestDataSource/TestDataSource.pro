################################################################################
#  BUILD DETAILS
################################################################################

TEMPLATE = app

CONFIG += console
CONFIG += c++11
CONFIG += warn_on
CONFIG += debug_and_release

CONFIG -= app_bundle
CONFIG -= qt

################################################################################
#  FILES
################################################################################

SOURCES += \
    src/testdatasource.cpp

HEADERS +=

INCLUDEPATH += $$PWD/../DashboardLibrary/src
INCLUDEPATH += $$PWD/../../include

TARGET = TestDataSource

################################################################################
#  OUTPUT DIRECTORIES
################################################################################

DESTDIR = ../../bin/
OBJECTS_DIR = ../../build/

Debug:DESTDIR = ../../bin/debug/
Debug:OBJECTS_DIR = ../../build/debug/

Release:DESTDIR = ../../bin/release/
Release:OBJECTS_DIR = ../../build/release/

# These directories are used to hold 3rd party libraries
# In Linux these should be installed in the system instead
# They should not be under version control
win32:LIBS += -L$$PWD/../../lib
win32:INCLUDEPATH += $$PWD/../../include

################################################################################
#  LIBRARIES
################################################################################

# This adds the project's output directory to the linker's search path
# It lets us link against projects that are part of the library
Debug:LIBS   += -L$$PWD/../../bin/debug
Release:LIBS += -L$$PWD/../../bin/release

LIBS += -lDashboardLibrary
LIBS += -lsqlite3

win32:LIBS += -lboost_system-mgw48-mt-1_58
win32:LIBS += -lws2_32

unix:LIBS += -lboost_system
unix:LIBS += -lboost_date_time
