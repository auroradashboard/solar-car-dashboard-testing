/******************************************************************************\
|  Includes                                                                    |
\******************************************************************************/

#include <chrono>
#include <thread>

#include <boost/asio.hpp>

#include <dashboardlibrary.h>

/******************************************************************************\
|  Constants                                                                   |
\******************************************************************************/

const char* default_listen_address = "0.0.0.0";
const char* default_multicast_address = "239.255.60.60";
const char* default_udp_port = "4876";

/******************************************************************************\
|  Functions                                                                   |
\******************************************************************************/

using namespace std;
using namespace Solaris;

int main() {
    // Load car config
    CAN::Network *network = new CAN::Network(CAR_CONFIG_FILE);
    UDP::Sender  *sender  = new UDP::Sender(default_multicast_address,
                                           default_udp_port);

    //
    // Create UDP::CAN_Frame
    //

    UDP::CAN_Frame *cf = new UDP::CAN_Frame;
    cf->protocol_version = 0x5472697469756; // This is a constant
    cf->bus_number = 0;
    cf->client_identifier = 0; // TODO Should be MAC

    cf->heartbeat = false;
    cf->settings = 0;
    cf->rtr = 0;

    cf->extended_id = 0;
    cf->can_identifier = 0x1000; // EuroTruck Plugin
    cf->length = 8;

    // Vehicle Speed - km/h
    cf->data.float_data[0] = 85.7f;

    //
    // Send the CAN_Frame several times
    //

    for(int i = 0; i < 10; i++) {
        cf->data.uint8_data[4] = (unsigned int) i;
        sender->send_can_frame(cf);

        std::this_thread::sleep_for(std::chrono::milliseconds(10));
    }

    delete cf;

    // Cleanup
    delete sender;
    delete network;

    return 0;
}
