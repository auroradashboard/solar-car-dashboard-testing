/******************************************************************************\
|  Includes                                                                    |
\******************************************************************************/

#include <iostream>

#include <boost/date_time.hpp>

#include <dashboardlibrary.h>

/******************************************************************************\
|  Constants                                                                   |
\******************************************************************************/

const char* default_tcp_ip = "localhost";
const char* default_tcp_port = "4879";

const char* speed_channel_name = "EuroTruck2 Plugin.General.Speed";
const char* tracker_channel_name = "MPPT 1.Information.Internal Temperature";

/******************************************************************************\
|  Functions                                                                   |
\******************************************************************************/

using namespace std;
using namespace Solaris;

void example_callback_function(TCP::Response* resp)
{
    if(resp->contains_channel(speed_channel_name))
    {
        std::vector<Buffer::ChannelValue*>* cv_vector = resp->get_channel_data(speed_channel_name);

        std::cout << "Speed: " << cv_vector->front()->get_float() << "km/h" << std::endl;
    }
}

int main()
{
    //
    // Set up
    //

    CAN::Network *network = new CAN::Network(CAR_CONFIG_FILE);
    TCP::Client *client = new TCP::Client(network, default_tcp_ip, default_tcp_port);

    std::vector<TCP::StreamClient*> stream_clients;

    //
    // Create Request
    //

    std::vector<CAN::Channel*> channels;

    channels.push_back(network->get_channel_by_name(speed_channel_name));
    channels.push_back(network->get_channel_by_name(tracker_channel_name));

    TCP::Request* req = new TCP::Request(TCP::REQ_STREAM, channels);

    //
    // Send Request and receive Response
    //

    std::cout << std::endl << req->create_string() << std::endl;

    client->send_request(req);
    TCP::Response* response = client->get_response(req);

    std::cout << std::endl << response->create_string() << std::endl;

    // Create a StreamClient object to receive the stream

    if(response->is_start_stream_response())
    {
        TCP::StreamClient* sc = new TCP::StreamClient(network, default_tcp_ip, response->get_port());
        sc->set_callback(example_callback_function);
        sc->start();
        stream_clients.push_back(sc);
    }

    //
    // Infinite loop to let stream listener thread run
    //

    for(;;)
    {
        // Should probably add some code to let you quit gracefully here
    }

    //
    // Clean up
    //

    while(stream_clients.size())
    {
        auto sc = stream_clients.back();
        sc->stop();
        stream_clients.pop_back();
        delete sc;
    }

    delete client;
    delete network;

    return 0;
}

