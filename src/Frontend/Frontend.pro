################################################################################
#  BUILD DETAILS
################################################################################

TEMPLATE = app

CONFIG += qt
CONFIG += c++11
CONFIG += warn_on
CONFIG += debug_and_release

QT += qml
QT += quick

# Default rules for deployment.
include(deployment.pri)

QMAKE_CXXFLAGS += -fPIC

################################################################################
#  FILES
################################################################################

SOURCES += src/main.cpp \
    src/fakedata.cpp \
    src/observer.cpp \
    #res/observee.cpp \
    src/guilink.cpp \
    src/subject.cpp \
    src/streamclient_qhelper.cpp

HEADERS += \
    src/fakedata.h \
    src/observer.h \
    #res/observee.h \
    src/guilink.h \
    src/subject.h \
    src/streamclient_qhelper.h

INCLUDEPATH += $$PWD/../DashboardLibrary/src
INCLUDEPATH += $$PWD/../../include

RESOURCES += res/qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

TARGET = Frontend

################################################################################
#  OUTPUT DIRECTORIES
################################################################################

DESTDIR = ../../bin/
OBJECTS_DIR = ../../build/

Debug:DESTDIR = ../../bin/debug/
Debug:OBJECTS_DIR = ../../build/debug/

Release:DESTDIR = ../../bin/release/
Release:OBJECTS_DIR = ../../build/release/

################################################################################
#  LIBRARIES
################################################################################

# These directories are used to hold 3rd party libraries
# In Linux these should be installed in the system instead
# They should not be under version control
win32:LIBS += -L$$PWD/../../lib
win32:INCLUDEPATH += $$PWD/../../include

# This adds the project's output directory to the linker's search path
# It lets us link against projects that are part of the library
Debug:LIBS   += -L$$PWD/../../bin/debug
Release:LIBS += -L$$PWD/../../bin/release

LIBS += -lDashboardLibrary

win32:LIBS += -lboost_system-mgw48-mt-1_58
win32:LIBS += -lws2_32

unix:LIBS += -lboost_system
unix:LIBS += -lboost_date_time

DISTFILES += \
    needle.png
