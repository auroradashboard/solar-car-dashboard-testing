#ifndef FAKEDATA_H
#define FAKEDATA_H

//#include "observee.h"
#include "observer.h"
#include "../src/subject.h"

class FakeData : public Subject<FakeData>
{
public:
    FakeData();

    double getSpeed(){return speed;}

    void start();

private:

    double speed;

};

#endif // FAKEDATA_H
