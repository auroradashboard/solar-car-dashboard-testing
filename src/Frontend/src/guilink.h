#ifndef GUILINK_H
#define GUILINK_H

#include "../src/observer.h"
#include "../src/fakedata.h"

#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlComponent>
#include <QDebug>
#include <QQmlProperty>

class GuiLink : public Observer<FakeData>
{
public:
    GuiLink();
    ~GuiLink();

    virtual void notify(FakeData* subject);
    void setRootObject(QObject* rootOb){rootObject = rootOb;}

private:

    QObject* rootObject;
    FakeData fd_oldData;

    void compare(FakeData* fd);
};

#endif // GUILINK_H
