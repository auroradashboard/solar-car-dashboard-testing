#ifndef STREAMCLIENT_QHELPER_H
#define STREAMCLIENT_QHELPER_H

#include <QObject>

#include <dashboardlibrary.h>

using namespace std;
using namespace Solaris;

class StreamClient_QHelper : public QObject
{
    Q_OBJECT
public:
    explicit StreamClient_QHelper(const char* ip,
                                  const char* port,
                                  std::function<void(Solaris::TCP::Response*)> callback,
                                  QObject *parent = 0);
    ~StreamClient_QHelper();

    void add_channel(const char* channel_name);

    void start();

signals:

public slots:
    void poll_one()
    {
        _sc->poll_one();
    }

private:
    CAN::Network *_network;
    TCP::Client *_client;
    TCP::StreamClient *_sc;
    const char* _ip;
    const char* _port;
    std::function<void(Solaris::TCP::Response*)> _stream_callback_function;

    std::vector<CAN::Channel*> _channels;
    std::vector<TCP::StreamClient*> _stream_clients;
};

#endif // STREAMCLIENT_QHELPER_H
