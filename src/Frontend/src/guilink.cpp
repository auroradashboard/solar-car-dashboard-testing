#include "guilink.h"
#include <QDebug>
#include <QQmlProperty>

#include "../src/fakedata.h"

GuiLink::GuiLink()
{

}


void GuiLink::notify(FakeData* subject)
{
    compare(subject);
}


void GuiLink::compare(FakeData* fd)
{
    qDebug() << "observee";

    FakeData newData = *fd;

    qDebug() << "new " << newData.getSpeed();
    qDebug() << "old " << fd_oldData.getSpeed();

    if(newData.getSpeed() != fd_oldData.getSpeed())
    {
        qDebug() << "setting";
        //rootObject->setProperty("speed", 400);
        qDebug() << "set";
        fd_oldData = newData;
    }
}
