/******************************************************************************\
|  Includes                                                                    |
\******************************************************************************/

#include <QGuiApplication>
#include <QtQml/QQmlApplicationEngine>
#include <QtQml/QQmlComponent>
#include <QDebug>
#include <QTimer>
#include <QQmlProperty>

#include <iostream>
#include <csignal>

#include <dashboardlibrary.h>

#include "streamclient_qhelper.h"

/******************************************************************************\
|  Constants                                                                   |
\******************************************************************************/

const char* default_tcp_ip = "127.0.0.1";
const char* default_tcp_port = "4879";

const char* speed_channel_name = "EuroTruck2 Plugin.Speed.Speed";
const char* cruise_channel_name = "EuroTruck2 Plugin.Speed.CruiseTarget";

using namespace std;
using namespace Solaris;

/******************************************************************************\
|  Global Variables                                                            |
\******************************************************************************/

QObject *rootObject;
QObject *qmlObject;

bool running = true;

/******************************************************************************\
|  Forward Declarations                                                        |
\******************************************************************************/

void stream_callback_function(TCP::Response* resp);

/******************************************************************************\
|  Functions                                                                   |
\******************************************************************************/

/**
 * @brief sigint_handler sets the global variable running to false
 * @param signal
 *
 * This should be attached to the signal using std::signal()
 */
void sigint_handler(int signal)
{
    running = false;
}

void stream_callback_function(TCP::Response* resp)
{
    if(resp->contains_channel(speed_channel_name))
    {
        std::vector<Buffer::ChannelValue*>* cv_vector = resp->get_channel_data(speed_channel_name);

        rootObject->setProperty("speed", cv_vector->front()->get_float());

    }

    if(resp->contains_channel(cruise_channel_name))
    {
        std::vector<Buffer::ChannelValue*>* cv_vector = resp->get_channel_data(cruise_channel_name);

        rootObject->setProperty("cruise", cv_vector->front()->get_float());

    }
}

int main(int argc, char *argv[])
{
    //
    // Set up
    //

    running = true;

    // Connect the SIGINT signal to our handler to set running = false
    // Lets the program be killed using Ctrl+C
    std::signal(SIGINT, sigint_handler);

    //
    // Start Qt Interface
    //

    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    rootObject = engine.rootObjects().first();
    qmlObject = rootObject->findChild<QObject*>("mainWindow");

    //
    // Create a StreamClient_QHelper to setup the network comms and integrate them with Qt
    //
    StreamClient_QHelper sc_qh(default_tcp_ip, default_tcp_port, stream_callback_function);

    sc_qh.add_channel(speed_channel_name);
    sc_qh.add_channel(cruise_channel_name);

    sc_qh.start();

    // TODO Look at moving this in to StreamClient_QHelper
    QTimer *timer = new QTimer(&sc_qh);
    QObject::connect(timer, SIGNAL(timeout()), &sc_qh, SLOT(poll_one()));
    timer->start(0);

    //
    // Run the GUI thread
    //

    return app.exec();
}
