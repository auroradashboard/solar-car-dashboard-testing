#include "streamclient_qhelper.h"

StreamClient_QHelper::StreamClient_QHelper(const char* ip,
                                           const char* port,
                                           std::function<void(Solaris::TCP::Response*)> callback,
                                           QObject *parent) : QObject(parent)
{
    _network = new CAN::Network(CAR_CONFIG_FILE);
    _client = new TCP::Client(_network, ip, port);
    _ip = ip;
    _port = port;
    _stream_callback_function = callback;
}

StreamClient_QHelper::~StreamClient_QHelper()
{
    TCP::Request *req = new TCP::Request(TCP::REQ_ENDSTREAM, _sc->get_port());
    _client->send_request(req);
}

void StreamClient_QHelper::add_channel(const char* channel_name)
{
    if(_network->get_channel_by_name(channel_name))
    {
        _channels.push_back(_network->get_channel_by_name(channel_name));
        std::cout << "Registered " << channel_name << std::endl;
    }
}

void StreamClient_QHelper::start()
{
    TCP::Request* req = new TCP::Request(TCP::REQ_STREAM, _channels);

    // Send Request and receive Response

    std::cout << std::endl << req->create_string() << std::endl;

    _client->send_request(req);
    TCP::Response* response = _client->get_response(req);

    std::cout << std::endl << response->create_string() << std::endl;

    // Create a StreamClient object to receive the stream

    if(response->is_start_stream_response())
    {
        _sc = new TCP::StreamClient(_network, _ip, response->get_port());
        _sc->set_callback(_stream_callback_function);
        _stream_clients.push_back(_sc);
        _sc->start();
    }
}
