#include "fakedata.h"
#include <QDebug>

#include <time.h>

FakeData::FakeData()
{

    speed = 10;
}


void FakeData::start()
{
    clock_t this_time = clock();
    clock_t last_time = this_time;
    double time_counter = 0;

    while(true)
    {
        this_time = clock();
        time_counter += (double(this_time - last_time));
        last_time = this_time;

        if(time_counter > (double)(5*CLOCKS_PER_SEC))
        {
            time_counter -= (double)(5*CLOCKS_PER_SEC);
            qDebug() << "clock";
            speed++;
            notifyObservers();
            qDebug() << "start";
        }



    }

}
