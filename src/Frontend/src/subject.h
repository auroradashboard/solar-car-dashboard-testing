#ifndef SUBJECT_H
#define SUBJECT_H

#include "../src/observer.h"
#include <vector>

template <class T>

class Subject
{
public:
    Subject(){}
    virtual ~Subject(){}

    void addObserver(Observer<T> &observer)
    {

        observers.push_back(&observer);

    }
    //void removeObserver(Observerobserver);
    void notifyObservers()
    {
        for(unsigned int i = 0 ; i < observers.size(); i++)
        {
            observers[i]->notify(static_cast<T*>(this));
        }
    }

private:
    std::vector<Observer<T> *> observers;
};

#endif // SUBJECT_H
