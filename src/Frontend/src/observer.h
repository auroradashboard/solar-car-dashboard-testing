#ifndef OBSERVER_H
#define OBSERVER_H

#include "observer.h"
#include <QObject>


template <class T>

class Observer
{
public:
    //Observer();
    //virtual ~Observer();
    virtual void notify(T* subject)=0;

private:

};

#endif // OBSERVER_H
