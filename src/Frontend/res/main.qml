import QtQuick 2.3
import QtQuick.Window 2.2
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtQuick.Extras 1.4

ApplicationWindow {

    property double speed: 50;
    property double charge: 80;
    property double power: 200;
    property double needleAngle: 0;
    property string backgroundClr: "#000000";
    property string currGear: "F";
    property bool engineWarning: false;
    property bool highbeamWarning: false;
    property double range: 300
    property double odometer: 123456
    property double cruise: 110

    property string white: "#ffffff"
    property bool leftIndicator: false
    property bool rightIndicator: false

    id: applicationWindow1
    visible: true
    width: 600
    height: 360
    title: qsTr("Hello World")
    color:backgroundClr

    Item{
        y: 45
        anchors.horizontalCenter: parent.horizontalCenter
        width:400
        height:45

        Image{
            sourceSize.height: 45
            sourceSize.width: 45
            fillMode: Image.PreserveAspectFit
            source: (leftIndicator >= true) ? "img/leftOn.png" : "img/leftOff.png"
        }
        Image{
            x: 355
            sourceSize.height: 45
            sourceSize.width: 45
            fillMode: Image.PreserveAspectFit
            source: (rightIndicator >= true) ? "img/rightOn.png" : "img/rightOff.png"
        }
    }


    Item {
    id: warningLights
    y: 16
    anchors.horizontalCenter: parent.horizontalCenter
    width: 400
    height: 50

        Row {
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            height:parent.height
            spacing: 10
            Image{
                height:parent.height
                fillMode: Image.PreserveAspectFit
                source: "img/engine.png"
                visible: engineWarning

            }
            Image{
                height:parent.height
                fillMode: Image.PreserveAspectFit
                source: "img/highbeams.png"
                visible: highbeamWarning

            }
        }
    }

    CircularGauge {
    id: chargeGauge
    x: 32
    y: 69
    width: 210
    height: 266
    antialiasing: false
    minimumValue: 0
    maximumValue: 100
    value: charge


    style: CircularGaugeStyle {
        tickmarkStepSize:50
        minimumValueAngle: 200
        maximumValueAngle: 340
        needle: Rectangle{
            //x: outerRadius * 0.3333
            //y:outerRadius
            //anchors.horizontalCenterOffset: -100
            implicitWidth: outerRadius * 0.03
            implicitHeight: outerRadius * 0.5
            antialiasing: true
        }
        foreground: Item{
            Rectangle {
                width: outerRadius * 0.2
                height: width
                   radius: width
                   color: "#f83535"
                  anchors.centerIn: parent
               }
            }
        }
    }

    CircularGauge {
        id: speedometer
        value: speed
        x: 168
        y: 69
        width: 264
        height: 260
        minimumValue: 0
        maximumValue: 220

        style: CircularGaugeStyle {
            tickmarkStepSize: 20

        }
    }


    CircularGauge {
        id: powerGauge
        x: 352
        y: 69
        width: 210
        height: 266
        antialiasing: false
        minimumValue: -400
        maximumValue: 400
        value: power


        style: CircularGaugeStyle {
            tickmarkStepSize:200
            minimumValueAngle: 160
            maximumValueAngle: 20
            needle: Rectangle{

                implicitWidth: outerRadius * 0.03
                implicitHeight: outerRadius * 0.5
                antialiasing: true
            }
            foreground: Item{
                Rectangle {
                       width: outerRadius * 0.2
                       height: width
                       radius: width
                       color: "#f83535"

                      anchors.centerIn: parent
                   }
            }
        }
    }

    Item{
        x: 275
        y: 206
        height: 50
        width: 50
        Column{
            anchors.centerIn: parent
            Text{text:"CRUISE:";color:white;anchors.horizontalCenter: parent.horizontalCenter}
            Text{text:cruise+qsTr(" KM/H");color:white;anchors.horizontalCenter: parent.horizontalCenter;font.pointSize: 14}
        }
    }


    Item{
        x: 200
        y: 228
        width: 200
        height: 75
        Column{
            anchors.centerIn: parent
            Text{
                text: qsTr("Range: ") + range + qsTr("KM")
                color: "#ffffff"
                anchors.horizontalCenter: parent.horizontalCenter
            }
            Text{
                text: qsTr("Total: ") + odometer + qsTr("KM")
                color: "#ffffff"
                anchors.horizontalCenter: parent.horizontalCenter

            }
        }
    }

    Button {
        id: button1
        x: 25
        y: 16
        visible:false
        text: qsTr("Button")
        onClicked: speed += 5;
    }

    Item{
        id:speedTxt
        x: 250
        y: 93
        width:100
        height:100
        visible: true
        Text{
            id: txt
            anchors.centerIn: parent
            text: speed
            anchors.verticalCenterOffset: -9

            color: "#FFFFFF"
            font.bold: true
            font.pointSize: 20
        }
        Text{
            anchors.centerIn: parent

            color: "#ffffff"

            text: qsTr("km/h")
            anchors.verticalCenterOffset: 15
        }
    }

    Item {
    id: gears
    x: 200
    y: 270
    width: 200
    height: 75

        Row {
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            spacing: 10

            Text{
                font.bold: true
                color: (currGear >= "F") ? "#f83535" : "#ffffff"
                font.pointSize: 16
                text: qsTr("F")
            }
            Text{
                color: (currGear >= "N") ? "#f83535" : "#ffffff"
                font.bold: true
                font.pointSize: 16
                text: qsTr("N")
            }
            Text{
                color: (currGear >= "P") ? "#f83535" : "#ffffff"
                font.bold: true
                font.pointSize: 16
                text: qsTr("P")
            }
            Text{
                color: (currGear >= "R") ? "#f83535" : "#ffffff"
                font.bold: true
                font.pointSize: 16
                text: qsTr("R")
            }
        }
    }



}
