#ifndef TCP_RESPONSE_H
#define TCP_RESPONSE_H

/******************************************************************************\
|  Includes                                                                    |
\******************************************************************************/

#include <map>

#include "../buffer.h"
#include "../can.h"
#include "../db.h"

#include "request.h"
#include "streamserver.h"

/******************************************************************************\
|  Data Types                                                                  |
\******************************************************************************/

namespace Solaris {
namespace TCP {

typedef enum Response_Type_Enum
{
    RESP_SINGLE,
    RESP_STREAM,
    RESP_STARTSTREAM,
    RESP_ENDSTREAM
} Response_Type_Enum;

class Response
{
public:
    Response(Solaris::CAN::Network* network, Request* req, std::string str_response);
    Response(Solaris::CAN::Network* network, std::string str_response);
    Response(Solaris::TCP::Request* req, Solaris::DB::Database* db);
    Response(Request* req, unsigned int port);
    Response(Solaris::TCP::StreamServer* ss, std::map<CAN::Channel*, Buffer::ChannelValue*> channel_values);

    bool is_start_stream_response();

    bool contains_channel(const char* name);

    unsigned int get_port();
    std::vector<Solaris::Buffer::ChannelValue*>* get_channel_data(const char* name);

    void load_data();
    std::string create_string();
private:

    std::string _get_payload_string(Solaris::CAN::Channel* c);
    unsigned int _get_payload_size(Solaris::CAN::Channel* c);

    Response_Type_Enum _type;
    Request* _req;
    Solaris::DB::Database* _db;
    Solaris::CAN::Network* _network;

    // Stream
    StreamServer* _stream_server;
    unsigned int _port;

    // Shared
    std::map<CAN::Channel*, std::vector<Buffer::ChannelValue*>*> _data;
};

}
}

#endif // TCP_RESPONSE_H
