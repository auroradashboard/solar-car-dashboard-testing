#include "streamclient.h"

#include <boost/bind.hpp>

using boost::asio::ip::tcp;
using Solaris::TCP::StreamClient;

const char* message_end_marker = "\n.\n";

/****************************************************************************\
|  Functions                                                                 |
\****************************************************************************/

StreamClient::StreamClient(Solaris::CAN::Network* network, const char* ip_address, unsigned int port)
    : _io_service(),
      _socket(_io_service),
      _resolver(_io_service),
      _query(tcp::v4(), ip_address, std::to_string(port))
{
    _network = network;
    _ip_address = ip_address;
    _port = port;
    _stopped = true;
}

StreamClient::~StreamClient()
{
    stop();
}

void StreamClient::start()
{
    std::cout << "TCP: StreamClient " << _port << " - Starting..." << std::endl;
    _stopped = false;

    //
    // Open connection to StreamServer
    //

    tcp::resolver::iterator endpoint_iter = _resolver.resolve(_query);
    tcp::resolver::iterator end;

    boost::system::error_code error = boost::asio::error::host_not_found;
    while(error && endpoint_iter != end)
    {
        _socket.close();
        _socket.connect(*endpoint_iter++, error);
    }

    if(error)
    {
        // Unable to get endpoint
        std::cout << "TCP: StreamClient " << _port << " - Unable to acquire endpoint" << std::endl;
        exit(0);
    }

    std::cout << "TCP: StreamClient " << _port << " - Connected to StreamServer" << std::endl;

    //
    // Start listening for data
    //

    _start_read();
}

void StreamClient::stop()
{
    _stopped = true;
    _socket.close();
    std::cout << "TCP: StreamClient " << _port << " - Stopped" << std::endl;
}

unsigned int StreamClient::get_port()
{
    return _port;
}

void StreamClient::poll_one()
{
    _io_service.poll_one();
}

void Solaris::TCP::StreamClient::_start_read()
{
    // TODO Get this working asynchronously

    boost::asio::async_read_until(
                _socket,
                _buffer,
                message_end_marker,
                boost::bind(&StreamClient::_handle_read_until,
                            this,
                            boost::asio::placeholders::error)
                );
}

void Solaris::TCP::StreamClient::_handle_read_until(const boost::system::error_code& ec)
{
    if(_stopped)
    {
        return;
    }

    if(!ec)
    {
        std::string str_response;

        // Turn the buffered data into a std::string
        boost::asio::streambuf::const_buffers_type bufs = _buffer.data();
        str_response = std::string(boost::asio::buffers_begin(bufs), boost::asio::buffers_begin(bufs) + _buffer.size());

        // Clear the buffer for the next round
        _buffer.consume(_buffer.size());

        // Create a Response object from the received string
        Solaris::TCP::Response* resp = new Solaris::TCP::Response(_network, str_response);

        do_callback(resp);

        _start_read();
    }
    else
    {
        std::cout << "TCP: StreamClient " << _port << " - Error - " << ec.message() << std::endl;
    }
}

void StreamClient::set_callback(std::function<void(Solaris::TCP::Response*)> callback)
{
    _callback = callback;
}

void Solaris::TCP::StreamClient::do_callback(Response* resp)
{
    // Send the Response to the callback function
    _callback(resp);
}
