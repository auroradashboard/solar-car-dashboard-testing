#include "client.h"

using boost::asio::ip::tcp;

#define MESSAGE_END_MARKER ("\n.\n")

/****************************************************************************\
|  Functions                                                                 |
\****************************************************************************/

Solaris::TCP::Client::Client(CAN::Network* network, const char* ip_address, const char* s_port)
    : _io_service(),
      _resolver(_io_service),
      _socket(_io_service),
      _query(tcp::v4(), ip_address, s_port)
{
    _network = network;
}

void Solaris::TCP::Client::send_request(Solaris::TCP::Request* req)
{
    tcp::resolver::iterator endpoint_iterator = _resolver.resolve(_query);
    tcp::resolver::iterator end;

    boost::system::error_code error = boost::asio::error::host_not_found;
    while(error && endpoint_iterator != end)
    {
        _socket.close();
        _socket.connect(*endpoint_iterator++, error);
    }

    if(error)
    {
        // Unable to get endpoint
        std::cout << "TCP Client: Unable to acquire endpoint" << std::endl;
        exit(0);
    }

    std::string message = req->create_string();

    boost::asio::write(_socket, boost::asio::buffer(message), boost::asio::transfer_all(), error);
}

Solaris::TCP::Response* Solaris::TCP::Client::get_response(Request* req)
{
    boost::asio::streambuf buffer;
    std::string str_response;

    // Read from TCP socket into stream buffer until the end of the respons
    boost::asio::read_until(_socket, buffer, MESSAGE_END_MARKER);

    // Turn it into a std::string
    boost::asio::streambuf::const_buffers_type bufs = buffer.data();
    str_response = std::string(boost::asio::buffers_begin(bufs), boost::asio::buffers_begin(bufs) + buffer.size());

    _socket.close();

    Solaris::TCP::Response* resp = new Solaris::TCP::Response(_network, req, str_response);

    return resp;
}
