#ifndef CLIENT_H
#define CLIENT_H

#include <boost/asio.hpp>

#include "request.h"
#include "response.h"
#include "../can.h"

namespace Solaris {
namespace TCP {

/******************************************************************************\
|  Data Types                                                                  |
\******************************************************************************/

class Client
{
public:
    Client(CAN::Network* network, const char* ip_address, const char* s_port);

    void send_request(Solaris::TCP::Request* req);
    Solaris::TCP::Response* get_response(Solaris::TCP::Request* req);

private:
    boost::asio::io_service _io_service;
    boost::asio::ip::tcp::resolver _resolver;
    boost::asio::ip::tcp::socket _socket;
    boost::asio::ip::tcp::resolver::query _query;

    CAN::Network* _network;
    unsigned int _port;
};

}
}

#endif // CLIENT_H
