#ifndef TCP_STREAMSERVER_H
#define TCP_STREAMSERVER_H

/******************************************************************************\
|  Includes                                                                    |
\******************************************************************************/

#include <vector>

#include <boost/asio.hpp>

#include "response.h"

#include "../buffer.h"
#include "../can.h"

namespace Solaris {
namespace TCP {

/******************************************************************************\
|  Data Types                                                                  |
\******************************************************************************/

class StreamServer
{
public:
    StreamServer(boost::asio::io_service& io_service, unsigned int port, std::vector<Solaris::CAN::Channel*>& channels);
    ~StreamServer();

    void start();
    void process_channel_group_value(Solaris::Buffer::ChannelGroupValue* cgv);

    unsigned int get_port();
private:
    Response* _create_response(std::map<CAN::Channel*, Buffer::ChannelValue*> channel_values);
    void _send_response(Response* resp);

    boost::asio::ip::tcp::endpoint _endpoint;
    boost::asio::ip::tcp::socket _socket;

    boost::asio::ip::tcp::acceptor *_acceptor;

    unsigned int _port;

    std::vector<Solaris::CAN::Channel*> _channels;
};

}
}

#endif // TCP_STREAMSERVER_H
