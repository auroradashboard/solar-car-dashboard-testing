#include "server.h"

#include <iostream>

using boost::asio::ip::tcp;

// TODO Make this a const char*
#define MESSAGE_END_MARKER ("\n.\n")

/****************************************************************************\
|  Public Functions                                                          |
\****************************************************************************/

Solaris::TCP::Server::Server(boost::asio::io_service& io_service, CAN::Network* network, DB::Database* db, std::atomic<bool>* running, const char* s_port)
    : _endpoint(tcp::v4(), atoi(s_port))
{
    _network = network;
    _db = db;
    _running = running;
    _port = s_port;
    _next_port_number = atoi(s_port) + 1;

    _acceptor = new tcp::acceptor(io_service, _endpoint);
}

Solaris::TCP::Server::~Server()
{
    for(auto& ss: _stream_servers)
    {
        delete ss;
    }

    delete _acceptor;
}

void Solaris::TCP::Server::start_accept()
{
    socket_pointer pointer(new boost::asio::ip::tcp::socket(_acceptor->get_io_service()));

    _acceptor->async_accept(*(pointer.get()), boost::bind(&Solaris::TCP::Server::_handle_accept, this, pointer, boost::asio::placeholders::error));
}

void Solaris::TCP::Server::stop_accept()
{
    _acceptor->cancel();
}

void Solaris::TCP::Server::run_stream_servers(Buffer::ChannelGroupValue* cgv)
{
    for(auto ss: _stream_servers)
    {
        ss->process_channel_group_value(cgv);
    }
}

/****************************************************************************\
|  Private Functions                                                         |
\****************************************************************************/

void Solaris::TCP::Server::_handle_accept(socket_pointer pointer, const boost::system::error_code& error)
{
    if(_running->load())
    {
        tcp::socket* socket = pointer.get();

        TCP::Request* req = _receive_request(socket);

        TCP::Response* resp = nullptr;

        if(req->is_stream_request())
        {
            TCP::StreamServer* ss = req->create_stream_server(socket->get_io_service(), _next_port_number);
            _stream_servers.push_back(ss);
            resp = new TCP::Response(req, _next_port_number);
            _send_response(socket, resp);
            ss->start();

            _next_port_number++;
        }

        if(req->is_end_stream_request())
        {
            unsigned int port = req->get_id();

            std::vector<TCP::StreamServer*>::iterator ss_iter = std::find_if(
                        _stream_servers.begin(),
                        _stream_servers.end(),
                        [port] (TCP::StreamServer* ss) { return ss->get_port() == port; } );

            if(ss_iter == _stream_servers.end())
            {
                // TODO Handle this error
                // FIXME
                std::cout << "TCP: Invalid EndStream Request" << std::endl;
            }
            else
            {
                TCP::StreamServer *ss = *ss_iter;
                _stream_servers.erase(ss_iter);
                delete ss;
            }
        }

        if(req->is_single_request())
        {
            resp = new TCP::Response(req, _db);
            _send_response(socket, resp);
        }

        // FIXME Handler error param
        start_accept();

        socket->close();
    }
    // TODO May need to close socket here
}

Solaris::TCP::Request* Solaris::TCP::Server::_receive_request(boost::asio::ip::tcp::socket* socket)
{
    boost::asio::streambuf buffer;
    std::string str_request;

    // Read from TCP socket into stream buffer until the end of the request
    boost::asio::read_until(*socket, buffer, MESSAGE_END_MARKER);

    // Turn it into a std::string
    boost::asio::streambuf::const_buffers_type bufs = buffer.data();
    str_request = std::string(boost::asio::buffers_begin(bufs), boost::asio::buffers_begin(bufs) + buffer.size());

    TCP::Request* req = new TCP::Request(_network, socket->remote_endpoint().address(), str_request);

    return req;
}


void Solaris::TCP::Server::_send_response(boost::asio::ip::tcp::socket* socket, Solaris::TCP::Response* response)
{
    std::string message = response->create_string();

    boost::system::error_code error;
    boost::asio::write(*socket, boost::asio::buffer(message), boost::asio::transfer_all(), error);
}
