#ifndef TCP_SERVER_H
#define TCP_SERVER_H

#include <atomic>

#include <boost/asio.hpp>

#include "request.h"
#include "response.h"

#include "../buffer.h"
#include "../can.h"
#include "../db.h"

namespace Solaris {
namespace TCP {

/****************************************************************************\
|  Data Types                                                                |
\****************************************************************************/

typedef boost::shared_ptr<boost::asio::ip::tcp::socket> socket_pointer;

class Server
{
public:
    Server(boost::asio::io_service& io_service, CAN::Network* network, DB::Database* db, std::atomic<bool>* running, const char* s_port);
    ~Server();

    void start_accept();
    void stop_accept();

    void run_stream_servers(Buffer::ChannelGroupValue* cgv);
private:
    void _handle_accept(socket_pointer pointer, const boost::system::error_code& error);

    Solaris::TCP::Request* _receive_request(boost::asio::ip::tcp::socket* socket);
    void _send_response(boost::asio::ip::tcp::socket* socket, Solaris::TCP::Response* response);

    boost::asio::ip::tcp::endpoint _endpoint;
    boost::asio::ip::tcp::acceptor *_acceptor;

    CAN::Network* _network;
    DB::Database* _db;
    std::atomic<bool>* _running;
    const char* _port;

    unsigned int _next_port_number;

    std::vector<Solaris::TCP::StreamServer*> _stream_servers;
};

}
}

#endif // TCP_SERVER_H
