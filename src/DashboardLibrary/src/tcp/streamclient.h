#ifndef TCP_STREAMCLIENT_H
#define TCP_STREAMCLIENT_H

/******************************************************************************\
|  Includes                                                                    |
\******************************************************************************/

#include <functional>
#include <thread>

#include <boost/asio.hpp>
#include <boost/bind.hpp>

#include "../can.h"
#include "response.h"

using boost::asio::ip::tcp;

/******************************************************************************\
|  Forward Declarations                                                        |
\******************************************************************************/

namespace Solaris {
namespace TCP {

class ClientAsyncConnection;

/******************************************************************************\
|  Data Types                                                                  |
\******************************************************************************/

typedef enum Thread_State {
    THREAD_STARTING,
    THREAD_RUNNING,
    THREAD_STOPPING,
    THREAD_STOPPED
} Thread_State;

class StreamClient
{
    //friend ClientAsyncConnection;

public:
    StreamClient(Solaris::CAN::Network* network, const char* ip_address, unsigned int port);
    ~StreamClient();

    void set_callback(std::function<void(Solaris::TCP::Response*)>);
    void do_callback(Response* resp);

    void start();
    void stop();

    unsigned int get_port();

    void poll_one();
private:
    void _start_read();
    void _handle_read_until(const boost::system::error_code& ec);

    boost::asio::io_service _io_service;
    boost::asio::ip::tcp::socket _socket;
    boost::asio::ip::tcp::resolver _resolver;
    tcp::resolver::query _query;

    const char* _ip_address;
    unsigned int _port;

    bool _stopped;

    boost::asio::streambuf _buffer;

    Solaris::CAN::Network* _network;

    std::function<void(Solaris::TCP::Response*)> _callback;
};

}
}

#endif // TCP_STREAMCLIENT_H
