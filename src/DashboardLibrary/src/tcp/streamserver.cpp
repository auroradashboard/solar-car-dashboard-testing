#include "streamserver.h"

#include <utility>

#include "response.h"

using boost::asio::ip::tcp;

Solaris::TCP::StreamServer::StreamServer(
        boost::asio::io_service& io_service,
        unsigned int port,
        std::vector<Solaris::CAN::Channel*>& channels)
    : _endpoint(tcp::v4(), port),
      _socket(io_service)
{
    _channels = std::vector<Solaris::CAN::Channel*>(channels);
    _port = port;

    std::cout << "TCP: StreamServer " << _port << " - Created!" << std::endl;
}

Solaris::TCP::StreamServer::~StreamServer()
{
    _socket.close();
    delete _acceptor;

    std::cout << "TCP: StreamServer " << _port << " - Destroyed!" << std::endl;
}

void Solaris::TCP::StreamServer::start()
{
    std::cout << "TCP: StreamServer " << _port << " - Accepting..." << std::endl;

    _acceptor = new tcp::acceptor(_socket.get_io_service(), _endpoint);
    _acceptor->accept(_socket);

    std::cout << "TCP: StreamServer " << _port << " - Accepted!" << std::endl;
    std::cout << "TCP: StreamServer " << _port << " - Running!" << std::endl;
}

void Solaris::TCP::StreamServer::process_channel_group_value(Solaris::Buffer::ChannelGroupValue* cgv)
{
    std::map<CAN::Channel*, Buffer::ChannelValue*> changed_values;

    for(auto &c: _channels)
    {
        if(c->channelGroup == cgv->_channelGroup)
        {
            changed_values[c] = cgv->get_channel_value(c);
        }
    }

    if(changed_values.size() > 0)
    {
        // At least one channel has an updated value
        Response* resp = _create_response(changed_values);
        _send_response(resp);
    }
}

unsigned int Solaris::TCP::StreamServer::get_port()
{
    return _port;
}

Solaris::TCP::Response* Solaris::TCP::StreamServer::_create_response(std::map<CAN::Channel*, Buffer::ChannelValue*> channel_values)
{
    Response* resp = new Response(this, channel_values);

    return resp;
}

void Solaris::TCP::StreamServer::_send_response(Response* resp)
{
    boost::system::error_code error = boost::asio::error::host_not_found;

    std::string message = resp->create_string();

#ifdef QT_DEBUG
    std::cout << "TCP: StreamServer " << _port << " - Sending packet..." << std::endl;
#endif

    boost::asio::write(_socket, boost::asio::buffer(message), boost::asio::transfer_all(), error);

    if(error)
    {
        std::cout << "TCP: Stream Server " << _port << " - Error: " << error.message() << std::endl;
    }
    else
    {
#ifdef QT_DEBUG
        std::cout << "TCP: StreamServer " << _port << " - Sent!" << std::endl;
#endif
    }
}
