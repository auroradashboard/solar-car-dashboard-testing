#include "response.h"

#include <string>
#include <utility>

using namespace Solaris::Buffer;
using namespace Solaris::CAN;
using namespace Solaris::DB;
using namespace Solaris::TCP;

Solaris::TCP::Response::Response(CAN::Network* network, Request* req, std::string str_response)
{
    _req = req;
    _network = network;

    std::istringstream ss(str_response);
    std::string line;

    std::getline(ss, line);

    std::string key = line.substr(0, line.find(": "));
    std::string value = line.substr(line.find(": ")+2, std::string::npos);

    if(key == "TYPE")
    {
        if(value == "Single")
        {
            _type = RESP_SINGLE;
        }

        if(value == "StartStream")
        {
            _type = RESP_STARTSTREAM;
        }

        if(value == "EndStream")
        {
            _type = RESP_ENDSTREAM;
        }
    }

    while(getline(ss, line))
    {
        if(line == ".")
        {
            // A line containing a single period marks the end of the message
            break;
        }

        switch(_type)
        {
        case RESP_SINGLE:
            key = line.substr(0, line.find(": "));
            value = line.substr(line.find(": ")+2, std::string::npos);

            if(key == "CHANNEL")
            {
                CAN::Channel* c = network->get_channel_by_name(value);
                std::vector<Buffer::ChannelValue*>* values = new std::vector<Buffer::ChannelValue*>();
                Buffer::Channel_Value_Type_Enum cv_type = Buffer::field_to_channel_value_type(c->field->type);

                std::getline(ss, line);

                std::string key = line.substr(0, line.find(": "));
                std::string value = line.substr(line.find(": ")+2, std::string::npos);

                if(key != "DATA")
                {
                    std::cout << std::endl << "Expected \"DATA:\" line. Aborting" << std::endl;
                    exit(0);
                }

                std::string data = value;

                while(value.find(","))
                {
                    std::string datum = data.substr(0, data.find(","));
                    data = data.substr(data.find(",")+1, std::string::npos);

                    Buffer::ChannelValue* cv = new Buffer::ChannelValue(cv_type, datum);
                    values->push_back(cv);
                }

                _data[c] = values;
            }

        case RESP_STARTSTREAM:
            key = line.substr(0, line.find(": "));
            value = line.substr(line.find(": ")+2, std::string::npos);

            if(key == "PORT")
            {
                _port = atoi(value.data());
            }
            break;

        case RESP_ENDSTREAM:
            // TODO
            break;

        case RESP_STREAM:
            // Should never occur
            break;
        }
    }
}

// This constructor is for handling responses from
Solaris::TCP::Response::Response(Solaris::CAN::Network* network, std::string str_response)
{
    _network = network;

    std::istringstream ss(str_response);
    std::string line;

    std::getline(ss, line);

    std::string key = line.substr(0, line.find(": "));
    std::string value = line.substr(line.find(": ")+2, std::string::npos);

    if(key == "TYPE")
    {
        if(value.compare("Single") == 0)
        {
            _type = RESP_SINGLE;
        }

        if(value.compare("Stream") == 0)
        {
            _type = RESP_STREAM;
        }

        if(value.compare("StartStream") == 0)
        {
            _type = RESP_STARTSTREAM;
        }

        if(value.compare("EndStream") == 0)
        {
            _type = RESP_ENDSTREAM;
        }
    }

    assert(_type == RESP_STREAM);

    while(getline(ss, line))
    {
        if(line == ".")
        {
            // A line containing a single period marks the end of the message
            break;
        }

        switch(_type)
        {
        case RESP_STREAM:
            key = line.substr(0, line.find(": "));
            value = line.substr(line.find(": ")+2, std::string::npos);

            if(key == "CHANNEL")
            {
                CAN::Channel* c = network->get_channel_by_name(value);
                std::vector<Buffer::ChannelValue*>* values = new std::vector<Buffer::ChannelValue*>();
                Buffer::Channel_Value_Type_Enum cv_type = Buffer::field_to_channel_value_type(c->field->type);

                std::getline(ss, line);

                std::string key = line.substr(0, line.find(": "));
                std::string value = line.substr(line.find(": ")+2, std::string::npos);

                if(key != "DATA")
                {
                    std::cout << std::endl << "Expected \"DATA:\" line. Aborting" << std::endl;
                    exit(0);
                }

                std::string data = value;

                while(data.find(","))
                {
                    std::string datum = data.substr(0, data.find(","));
                    //data = data.substr(data.find(",")+1, std::string::npos);
                    // Remove length of string + comma
                    data.erase(0, datum.length());

                    Buffer::ChannelValue* cv = new Buffer::ChannelValue(cv_type, datum);
                    values->push_back(cv);
                }

                _data[c] = values;
            }
            break;

        case RESP_ENDSTREAM:

            break;

        default:
            break;
        }
    }
}

Solaris::TCP::Response::Response(Request* req, Database* db)
{
    _req = req;
    _db = db;

    switch(_req->_type)
    {
    case TCP::REQ_SINGLE:
        _type = RESP_SINGLE;
        break;

    case TCP::REQ_STREAM:
        _type = RESP_STARTSTREAM;
        break;

    case TCP::REQ_ENDSTREAM:
        _type = RESP_ENDSTREAM;
        break;
    }

    load_data();
}

Solaris::TCP::Response::Response(Request* req, unsigned int port)
{
    assert(req->_type == REQ_STREAM);

    _req = req;
    _port = port;
    _type = RESP_STARTSTREAM;
}

Solaris::TCP::Response::Response(Solaris::TCP::StreamServer* ss, std::map<CAN::Channel*, Buffer::ChannelValue*> channel_values)
{
    _type = RESP_STREAM;
    _req = nullptr;
    _db = nullptr;
    _stream_server = ss;

    for(auto &kv: channel_values)
    {
        auto vec = new std::vector<Buffer::ChannelValue*>();
        vec->push_back(kv.second);

        _data[kv.first] = vec;
    }
}

bool Solaris::TCP::Response::is_start_stream_response()
{
    return (_type == RESP_STARTSTREAM);
}

bool Solaris::TCP::Response::contains_channel(const char* name)
{
    CAN::Channel* c = _network->get_channel_by_name(name);

    return (_data.find(c) != _data.end());
}

unsigned int Solaris::TCP::Response::get_port()
{
    return _port;
}

std::vector<Solaris::Buffer::ChannelValue*>* Solaris::TCP::Response::get_channel_data(const char* name)
{
    CAN::Channel* c = _network->get_channel_by_name(name);

    return _data[c];
}

void Solaris::TCP::Response::load_data()
{
    for(auto &c: _req->_channels)
    {
        std::vector<Buffer::ChannelValue*>* values = _db->select(c, _req->_time_period);

        _data[c] = values;
    }
}

std::string Solaris::TCP::Response::create_string()
{
    std::string message = "";

    //
    // Type
    //

    message += "TYPE: ";
    switch(_type)
    {
    case RESP_SINGLE:
        message += "Single";

        /*
        //
        // Channels
        //

        for(auto &c: _req->_channels)
        {
            message += "CHANNEL: ";
            message += c->channelGroup->deviceInstance->name;
            message += ".";
            message += c->channelGroup->frame->name;
            message += ".";
            message += c->field->name;
            message += "\n";
            message += "DATA: ";
            message += _get_payload_string(c);
            message += "\n";
        }
        */
        break;

    case RESP_STREAM:
        message += "Stream";
        break;

    case RESP_STARTSTREAM:
        message += "StartStream";
        message += "\n";
        message += "PORT: ";
        message += std::to_string(_port);

        break;

    case RESP_ENDSTREAM:
        message += "EndStream";

        // TODO
        break;
    }

    message += "\n";

    //
    // Channels
    //

    for(auto &kv: _data)
    {
        message += "CHANNEL: ";
        message += kv.first->channelGroup->deviceInstance->name;
        message += ".";
        message += kv.first->channelGroup->frame->name;
        message += ".";
        message += kv.first->field->name;
        message += "\n";
        message += "DATA: ";
        message += _get_payload_string(kv.first);
        message += "\n";
    }


    message += ".\n";

    return message;
}

std::string Solaris::TCP::Response::_get_payload_string(Solaris::CAN::Channel* c)
{
    std::string message = "";

    // Turn each ChannelValue's data into a string and append it
    for(auto &cv: *_data[c])
    {
        message += cv->get_sql_value();
        message += ",";
    }

    // Note - the trailing comma is significant and is used for parsing the data
    // See TCP::Response(network, req, str_response)

    return message;
}

unsigned int Solaris::TCP::Response::_get_payload_size(Solaris::CAN::Channel* c)
{
    return _data[c]->size();
}
