#ifndef TCP_REQUEST_H
#define TCP_REQUEST_H

/******************************************************************************\
|  Includes                                                                    |
\******************************************************************************/

#include <string>
#include <vector>

#include "boost/asio.hpp"
#include "boost/date_time/posix_time/posix_time.hpp"

#include "../can.h"

namespace Solaris {
namespace TCP {

/******************************************************************************\
|  Forward Declarations                                                        |
\******************************************************************************/

class Response;
class StreamServer;

/******************************************************************************\
|  Data Types                                                                  |
\******************************************************************************/

typedef enum Request_Type_Enum
{
    REQ_SINGLE,
    REQ_STREAM,
    REQ_ENDSTREAM
} Request_Type_Enum;

typedef enum Request_Resolution_Enum
{
    RR_RAW,
    RR_SECOND,
    RR_MINUTE,
    RR_HOUR,
    RR_DAY
} Request_Resolution_Enum;

class Request
{
    friend class Response;
public:
    Request(boost::posix_time::time_period tp, Request_Type_Enum type, Request_Resolution_Enum res, std::vector<Solaris::CAN::Channel*> channels);
    Request(Request_Type_Enum type, Request_Resolution_Enum res, std::vector<Solaris::CAN::Channel*> channels);
    Request(Request_Type_Enum type, std::vector<Solaris::CAN::Channel*> channels);
    Request(Request_Type_Enum type, unsigned int port);
    Request(CAN::Network* network, boost::asio::ip::address ip_address, std::string req);

    std::string create_string();

    bool is_stream_request();
    bool is_end_stream_request();
    bool is_single_request();

    unsigned int get_id();

    Solaris::TCP::StreamServer* create_stream_server(boost::asio::io_service& io_service, unsigned int port);

private:
    Request_Type_Enum _type;

    // Single Request
    boost::posix_time::time_period _time_period;
    Request_Resolution_Enum _resolution;

    // Stream Request
    const char* _port;

    // EndStream Request
    unsigned int _id;

    // Shared
    boost::asio::ip::address _ip_address;
    std::vector<Solaris::CAN::Channel*> _channels;
};

/******************************************************************************\
|  Functions                                                                   |
\******************************************************************************/

Request_Resolution_Enum string_to_resolution_enum(std::string str);

}
}

#endif // TCP_REQUEST_H
