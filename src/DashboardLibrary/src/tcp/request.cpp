#include "request.h"

#include <iostream>

#include "streamserver.h"

using namespace boost::posix_time;

Solaris::TCP::Request::Request(boost::posix_time::time_period tp, Request_Type_Enum type, Request_Resolution_Enum res, std::vector<Solaris::CAN::Channel*> channels)
    : _time_period(tp)
{
    _type = type;
    _resolution = res;
    _channels = channels;
}

Solaris::TCP::Request::Request(Request_Type_Enum type, Request_Resolution_Enum res, std::vector<Solaris::CAN::Channel*> channels)
    : _time_period(microsec_clock::universal_time(), microsec_clock::universal_time())
{
    _type = type;
    _resolution = res;
    _channels = channels;
}

Solaris::TCP::Request::Request(Request_Type_Enum type, std::vector<Solaris::CAN::Channel*> channels)
    : _time_period(microsec_clock::universal_time(), microsec_clock::universal_time())
{
    _type = type;
    _resolution = RR_RAW;
    _channels = channels;
}

Solaris::TCP::Request::Request(Request_Type_Enum type, unsigned int port)
    : _time_period(microsec_clock::universal_time(), microsec_clock::universal_time())
{
    _type = type;
    _id = port;
}

Solaris::TCP::Request::Request(CAN::Network* network, boost::asio::ip::address ip_address, std::string req)
    : _time_period(microsec_clock::universal_time(), microsec_clock::universal_time())
{
    _ip_address = ip_address;

    std::istringstream ss(req);
    std::string line;

    std::getline(ss, line);

    std::string key = line.substr(0, line.find(": "));
    std::string value = line.substr(line.find(": ")+2, std::string::npos);

    if(key == "TYPE")
    {
        if(value == "Single")
        {
            _type = REQ_SINGLE;
        }

        if(value == "Stream")
        {
            _type = REQ_STREAM;
        }

        if(value == "EndStream")
        {
            _type = REQ_ENDSTREAM;
        }
    }

    while(getline(ss, line))
    {
        if(line == ".")
        {
            // A line containing a single period marks the end of the message
            break;
        }

        switch(_type)
        {
        case REQ_SINGLE:
            key = line.substr(0, line.find(": "));
            value = line.substr(line.find(": ")+2, std::string::npos);

            if(key == "TIMEPERIOD")
            {
                // Create duration of given length
                time_duration duration = duration_from_string(value);

                // Create time_period as [now, now+duration)
                _time_period = time_period(microsec_clock::universal_time(), duration);

                // Shift it to make it [now - duration, now)
                // eg. a duration going backwards from now
                _time_period.shift(-duration);
            }
            else if(key == "STARTENDTIME")
            {
                // Split value string at comma to get start/end time strings
                std::string s_start = value.substr(0, value.find(","));
                std::string s_end = value.substr(value.find(",")+1, std::string::npos);

                // Create start/end ptimes from strings
                ptime p_start = from_iso_string(s_start);
                ptime p_end = from_iso_string(s_end);

                // Create time_period from ptimes
                _time_period = time_period(p_start, p_end);
            }
            else if(key == "RESOLUTION")
            {
                _resolution = string_to_resolution_enum(value);
            }
            else if(key == "CHANNEL")
            {
                CAN::Channel* c = network->get_channel_by_name(value);

                if(c != nullptr)
                {
                    _channels.push_back(c);
                }
            }

        break;

        case REQ_STREAM:
            key = line.substr(0, line.find(": "));
            value = line.substr(line.find(": ")+2, std::string::npos);

            if(key == "PORT")
            {
                _port = value.data();
            }
            else if(key == "CHANNEL")
            {
                CAN::Channel* c = network->get_channel_by_name(value);

                if(c != nullptr)
                {
                    _channels.push_back(c);
                }
            }
        break;

        case REQ_ENDSTREAM:
            key = line.substr(0, line.find(": "));
            value = line.substr(line.find(": ")+2, std::string::npos);

            if(key == "ID")
            {
                _id = atoi(value.data());
            }
            break;
        }
    }
}

std::string Solaris::TCP::Request::create_string()
{
    std::string message = "";

    //
    // Type
    //

    message += "TYPE: ";
    switch(_type)
    {
    case REQ_SINGLE:
        message += "Single";
        break;
    case REQ_STREAM:
        message += "Stream";
        break;
    case REQ_ENDSTREAM:
        message += "EndStream";
        break;
    }
    message += "\n";

    if(_type == REQ_SINGLE)
    {
        //
        // Start/End Time
        //

        message += "STARTENDTIME: ";
        message += to_iso_string(_time_period.begin());
        message += ",";
        message += to_iso_string(_time_period.end());
        message += "\n";
    }

    if(_type == REQ_SINGLE || _type == REQ_STREAM)
    {
        //
        // Resolution
        //

        message += "RESOLUTION: ";

        switch(_resolution)
        {
        case RR_RAW:
            message += "Raw";
            break;
        case RR_SECOND:
            message += "Second";
            break;
        case RR_MINUTE:
            message += "Minute";
            break;
        case RR_HOUR:
            message += "Hour";
            break;
        case RR_DAY:
            message += "Day";
            break;
        }
        message += "\n";

        //
        // Channels
        //

        for(auto &c: _channels)
        {
            message += "CHANNEL: ";
            message += c->channelGroup->deviceInstance->name;
            message += ".";
            message += c->channelGroup->frame->name;
            message += ".";
            message += c->field->name;
            message += "\n";
        }
    }

    if(_type == REQ_ENDSTREAM)
    {
        //
        // Stream ID/Port
        //
        message += "ID: ";
        message += std::to_string(_id);
        message += "\n";
    }

    //
    // End Marker
    //

    message += ".\n";

    return message;
}

Solaris::TCP::Request_Resolution_Enum Solaris::TCP::string_to_resolution_enum(std::string str)
{
    if(str == "Day")
    {
        return RR_DAY;
    }
    else if(str == "Hour")
    {
        return RR_HOUR;
    }
    else if(str == "Minute")
    {
        return RR_MINUTE;
    }
    else if(str == "Second")
    {
        return RR_SECOND;
    }
    else
    {
        return RR_RAW;
    }
}



bool Solaris::TCP::Request::is_stream_request()
{
    return (_type == REQ_STREAM);
}

bool Solaris::TCP::Request::is_end_stream_request()
{
    return (_type == REQ_ENDSTREAM);
}

bool Solaris::TCP::Request::is_single_request()
{
    return (_type == REQ_SINGLE);
}

unsigned int Solaris::TCP::Request::get_id()
{
    return _id;
}

Solaris::TCP::StreamServer* Solaris::TCP::Request::create_stream_server(boost::asio::io_service& io_service, unsigned int port)
{
    Solaris::TCP::StreamServer* ss = new Solaris::TCP::StreamServer(io_service, port, _channels);
    return ss;
}
