#include "channelgroupbuffer.h"

using namespace Solaris::Buffer;

Solaris::Buffer::ChannelGroupBuffer::ChannelGroupBuffer(CAN::ChannelGroup *cg)
{
    _channelGroup = cg;
    // TODO Look at better ways to set the capacity
    _buffer.set_capacity(cg->frame->frequency);

}

bool Solaris::Buffer::ChannelGroupBuffer::is_full()
{
    return _buffer.full();
}

bool Solaris::Buffer::ChannelGroupBuffer::is_empty()
{
    return _buffer.empty();
}

ChannelGroupValue* Solaris::Buffer::ChannelGroupBuffer::pop()
{
    ChannelGroupValue *cgv = _buffer.front();
    _buffer.pop_front();

    return cgv;
}

void Solaris::Buffer::ChannelGroupBuffer::push(Buffer::ChannelGroupValue* cgv)
{
    _buffer.push_back(cgv);
}
