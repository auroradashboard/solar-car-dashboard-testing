#include "channelvalue.h"

#include <cassert>
#include <iostream>
#include <string>

#include "../dashboardlibrary_globals.h"

using namespace Solaris::Buffer;
using namespace Solaris::CAN;

Solaris::Buffer::ChannelValue::ChannelValue(Channel_Value_Type_Enum t)
{
    _type = t;
}

Solaris::Buffer::ChannelValue::ChannelValue(Channel_Value_Type_Enum t, std::string str)
{
    _type = t;

    switch(_type)
    {
    case CVT_BOOL:
        if(str == "0")
        {
            _bool_data = false;
        }
        break;

    case CVT_FLOAT:
        _float_data = stof(str);
        break;

    case CVT_INT:
        _int_data = stol(str);
        break;

    case CVT_UINT:
        _uint_data = stoul(str);
        break;
    }
}

void Solaris::Buffer::ChannelValue::store_bool(bool d)
{
    assert(_type == CVT_BOOL);
    _bool_data = d;
}

void Solaris::Buffer::ChannelValue::store_float(float d)
{
    assert(_type == CVT_FLOAT);
    _float_data = d;
}

void Solaris::Buffer::ChannelValue::store_int(int d)
{
    assert(_type == CVT_INT);
    _int_data = d;
}

void Solaris::Buffer::ChannelValue::store_uint(unsigned int d)
{
    assert(_type == CVT_UINT);
    _uint_data = d;
}

bool Solaris::Buffer::ChannelValue::get_bool()
{
    assert(_type == CVT_BOOL);
    return _bool_data;
}

float Solaris::Buffer::ChannelValue::get_float()
{
    assert(_type == CVT_FLOAT);
    return _float_data;
}

int Solaris::Buffer::ChannelValue::get_int()
{
    assert(_type == CVT_INT);
    return _int_data;
}

unsigned int Solaris::Buffer::ChannelValue::get_uint()
{
    assert(_type == CVT_UINT);
    return _uint_data;
}

Solaris::Buffer::Channel_Value_Type_Enum Solaris::Buffer::ChannelValue::get_type()
{
    return _type;
}

std::string Solaris::Buffer::ChannelValue::get_sql_value()
{
    switch(_type)
    {
    case CVT_BOOL:
        if(_bool_data)
        {
            return "1";
        }
        else
        {
            return "0";
        }
        break;

    case CVT_FLOAT:
        return std::to_string(_float_data);
        break;

    case CVT_INT:
        return std::to_string(_int_data);
        break;

    case CVT_UINT:
        return std::to_string(_uint_data);
        break;

    default:
        return "";
    }
}

/******************************************************************************\
|  Non-Member Functions                                                        |
\******************************************************************************/

Solaris::Buffer::Channel_Value_Type_Enum Solaris::Buffer::field_to_channel_value_type(Field_Type_Enum ft)
{
    switch(ft)
    {
    case F_BOOL:
        return CVT_BOOL;
        break;

    case F_FLOAT32:
    case F_FLOAT64:
        return CVT_FLOAT;
        break;

    case F_INT8:
    case F_INT16:
    case F_INT32:
        return CVT_INT;
        break;

    case F_UINT8:
    case F_UINT16:
    case F_UINT32:
        return CVT_INT;
        break;

    default:
        // Unkown type
        std::cerr << "field_to_channel_value_type() Error - Unknown Type" << std::endl;
        exit(0);
        break;
    }
}
