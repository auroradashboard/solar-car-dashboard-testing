#ifndef BUFFER_CHANNELVALUE_H
#define BUFFER_CHANNELVALUE_H

#include <typeinfo>

#include "../can.h"

namespace Solaris {
namespace Buffer {

/******************************************************************************\
|  Data Types                                                                  |
\******************************************************************************/

/**
 * @brief TODO
 */

typedef enum Channel_Value_Type_Enum {
    CVT_BOOL,
    CVT_FLOAT,
    CVT_INT,
    CVT_UINT
} Channel_Value_Type_Enum;

class ChannelValue
{
public:
    ChannelValue(Channel_Value_Type_Enum t);
    ChannelValue(Channel_Value_Type_Enum t, std::string str);

    void store_bool(bool d);
    void store_float(float d);
    void store_int(int d);
    void store_uint(unsigned int d);

    bool get_bool();
    float get_float();
    int get_int();
    unsigned int get_uint();

    std::string get_sql_value();

    Channel_Value_Type_Enum get_type();
private:
    Channel_Value_Type_Enum _type;

    union
    {
        bool _bool_data;
        float _float_data;
        int _int_data;
        unsigned int _uint_data;
    };
};

/******************************************************************************\
|  Functions                                                                   |
\******************************************************************************/

Channel_Value_Type_Enum field_to_channel_value_type(CAN::Field_Type_Enum ft);

}
}

#endif // BUFFER_CHANNELVALUE_H
