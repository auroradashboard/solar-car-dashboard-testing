#ifndef BUFFER_CHANNELGROUPBUFFER_H
#define BUFFER_CHANNELGROUPBUFFER_H

#include <boost/circular_buffer.hpp>

#include "channelgroupvalue.h"

#include "../can.h"

namespace Solaris {
namespace Buffer {

class ChannelGroupBuffer
{
public:
    ChannelGroupBuffer(CAN::ChannelGroup *cg);

    bool is_full();
    bool is_empty();

    Buffer::ChannelGroupValue* pop();
    void push(Buffer::ChannelGroupValue* cgv);

    // FIXME Should be private
    CAN::ChannelGroup *_channelGroup;

private:
    boost::circular_buffer<Buffer::ChannelGroupValue*> _buffer;
};

}
}

#endif // BUFFER_CHANNELGROUPBUFFER_H
