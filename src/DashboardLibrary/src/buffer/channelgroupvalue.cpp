#include "channelgroupvalue.h"

#include <algorithm>
#include <boost/date_time.hpp>

Solaris::Buffer::ChannelGroupValue::ChannelGroupValue(CAN::ChannelGroup *cg, boost::posix_time::ptime t)
{
    _channelGroup = cg;
    _timestamp = t;
}

Solaris::Buffer::ChannelGroupValue::~ChannelGroupValue()
{
    for(auto &kv: _channelValues)
    {
        delete kv.second;
    }
}

std::string Solaris::Buffer::ChannelGroupValue::get_sql_insert_statement()
{
    std::string query = _channelGroup->getInsertStatement();

    // to_iso_extended_string() produce dates as 2002-01-31T10:00:01,123456789
    // sqlite's datetime() needs dates formatted 2002-01-31 10:00:01.123456789

    std::string datetime = to_iso_extended_string(_timestamp);
    std::replace(datetime.begin(), datetime.end(), ',', '.');
    std::replace(datetime.begin(), datetime.end(), 'T', ' ');
    datetime.insert(0, "'");
    datetime.append("'");

    std::size_t i = query.find_first_of("?");
    query.erase(i, 1);
    query.insert(i, datetime);
    //std::replace(query.begin(), query.at(i), "?", datetime)

    // Replace remaining question marks in query with values
    for(auto &cv: _channelValues)
    {
        std::size_t i = query.find_first_of("?");
        query.erase(i, 1);
        query.insert(i, cv.second->get_sql_value());
    }

    return query;
}

void Solaris::Buffer::ChannelGroupValue::insert(std::pair<CAN::Channel*, ChannelValue*> p)
{
    _channelValues.insert(p);
}

Solaris::Buffer::ChannelValue* Solaris::Buffer::ChannelGroupValue::get_channel_value(CAN::Channel* c)
{
    return _channelValues[c];
}
