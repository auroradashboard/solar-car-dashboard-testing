#ifndef BUFFER_CHANNELGROUPVALUE_H
#define BUFFER_CHANNELGROUPVALUE_H

#include <ctime>
#include <map>

#include <boost/date_time/posix_time/posix_time.hpp>

#include "channelvalue.h"

#include "../can.h"

namespace Solaris {
namespace Buffer {

class ChannelGroupValue
{
public:
    ChannelGroupValue(CAN::ChannelGroup *cg, boost::posix_time::ptime t);
    ~ChannelGroupValue();

    void insert(std::pair<CAN::Channel*, Solaris::Buffer::ChannelValue*> p);

    std::string get_sql_insert_statement();

    ChannelValue* get_channel_value(CAN::Channel* c);

    // FIXME Should be private
    CAN::ChannelGroup *_channelGroup;

private:
    boost::posix_time::ptime _timestamp;
    std::map<CAN::Channel*, ChannelValue*> _channelValues;
};

}
}

#endif // BUFFER_CHANNELGROUPVALUE_H
