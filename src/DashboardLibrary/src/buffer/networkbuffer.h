#ifndef BUFFER_NETWORKBUFFER_H
#define BUFFER_NETWORKBUFFER_H

#include <vector>

#include "channelgroupbuffer.h"
#include "channelgroupvalue.h"

#include "../can.h"
#include "../udp.h"

namespace Solaris {
namespace Buffer {

class NetworkBuffer
{

public:
    NetworkBuffer(Solaris::CAN::Network* network);
    ~NetworkBuffer();

    ChannelGroupValue* process_can_frame(UDP::CAN_Frame* cf, boost::posix_time::ptime t);
    ChannelGroupValue* process_can_frame(UDP::CAN_Frame* cf);

    void buffer(Buffer::ChannelGroupValue *cgv);

    // FIXME Should be private, but needed by DB::Database
    std::vector<Buffer::ChannelGroupBuffer*> _buffers;

private:
    Solaris::CAN::Network *_network;
};

}
}

#endif // BUFFER_NETWORKBUFFER_H
