#include "networkbuffer.h"

using namespace Solaris::Buffer;
using namespace Solaris::CAN;

Solaris::Buffer::NetworkBuffer::NetworkBuffer(Solaris::CAN::Network* network)
{
    _network = network;

    for(auto &di: _network->deviceInstances)
    {
        for(auto &cg: di->channelGroups)
        {
            _buffers.push_back(new ChannelGroupBuffer(cg));
        }
    }
}

Solaris::Buffer::NetworkBuffer::~NetworkBuffer()
{
    for(auto &cgb: _buffers)
    {
        delete cgb;
    }
}

ChannelGroupValue* Solaris::Buffer::NetworkBuffer::process_can_frame(
        UDP::CAN_Frame* cf, boost::posix_time::ptime t)
{
    ChannelGroup *cg = _network->getChannelGroup(cf->can_identifier);

    if(cg == nullptr)
    {
        // TODO Handle this better
        return nullptr;
    }

    ChannelGroupValue *cgv = new ChannelGroupValue(cg, t);

    if(cgv == nullptr)
    {
        // TODO Handle this better
        return nullptr;
    }

    for(auto &c: cg->channels)
    {
        ChannelValue *cv;
        switch(c->field->type)
        {
        case F_UINT8:
            cv = new ChannelValue(CVT_UINT);
            cv->store_uint(cf->data.uint8_data[c->field->startBit / 8]);
            break;

        case F_UINT16:
            cv = new ChannelValue(CVT_UINT);
            cv->store_uint(cf->data.uint16_data[c->field->startBit / 16]);
            break;

        case F_UINT32:
            cv = new ChannelValue(CVT_UINT);
            cv->store_uint(cf->data.uint32_data[c->field->startBit / 32]);
            break;


        case F_INT8:
            cv = new ChannelValue(CVT_INT);
            cv->store_int(cf->data.uint8_data[c->field->startBit / 8]);
            break;

        case F_INT16:
            cv = new ChannelValue(CVT_INT);
            cv->store_int(cf->data.uint16_data[c->field->startBit / 16]);
            break;

        case F_INT32:
            cv = new ChannelValue(CVT_INT);
            cv->store_int(cf->data.uint32_data[c->field->startBit / 32]);
            break;


        case F_FLOAT32:
            cv = new ChannelValue(CVT_FLOAT);
            cv->store_float(cf->data.float_data[c->field->startBit / 32]);
            break;

        case F_FLOAT64:
            cv = new ChannelValue(CVT_FLOAT);
            cv->store_float(cf->data.double_data);
            break;


        case F_BOOL:
            cv = new ChannelValue(CVT_BOOL);
            cv->store_bool((cf->data.uint64_data >> c->field->startBit) & c->field->width);
            break;

        case F_NO_TYPE:
            continue;
        }

        if(cv != nullptr)
        {
            cgv->insert(std::pair<Channel*, ChannelValue*>(c, cv));
        }
    }

    return cgv;
}

ChannelGroupValue* Solaris::Buffer::NetworkBuffer::process_can_frame(UDP::CAN_Frame* cf)
{
    return process_can_frame(cf, boost::posix_time::microsec_clock::universal_time());
}

void Solaris::Buffer::NetworkBuffer::buffer(ChannelGroupValue *cgv)
{
    for(auto &cgb: _buffers)
    {
        if(cgb->_channelGroup == cgv->_channelGroup)
        {
            cgb->push(cgv);
        }
    }
}
