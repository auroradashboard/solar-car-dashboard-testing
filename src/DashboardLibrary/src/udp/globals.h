#ifndef UDP_GLOABLS_H
#define UDP_GLOABLS_H

#include <cstdint>

namespace Solaris {
namespace UDP {

/****************************************************************************\
|  Data Types                                                                |
\****************************************************************************/

typedef union CAN_Frame
{
    struct
    {
        volatile uint64_t                               : 8;
        volatile uint64_t protocol_version              : 52;
        volatile uint64_t bus_number                    : 4;

        volatile uint8_t                                : 0;

        volatile uint64_t                               : 8;
        volatile uint64_t client_identifier             : 56;

        volatile uint8_t                                : 0;

        volatile uint32_t can_identifier                : 32;

        volatile uint8_t                                : 0;

        volatile uint8_t heartbeat                      : 1;
        volatile uint8_t settings                       : 1;
        volatile uint8_t                                : 4;
        volatile uint8_t rtr                            : 1;
        volatile uint8_t extended_id                    : 1;

        volatile uint8_t                                : 0;

        volatile uint8_t length                         : 8;

        volatile uint8_t                                : 0;

        volatile union
        {
            volatile uint8_t  uint8_data[8];
            volatile uint16_t uint16_data[4];
            volatile uint32_t uint32_data[2];
            volatile uint64_t uint64_data;

            volatile int8_t   int8_data[8];
            volatile int16_t  int16_data[4];
            volatile int32_t  int32_data[2];
            volatile int64_t  int64_data;

            volatile float float_data[2];
            volatile double double_data;
        } data;
    };
    uint8_t bytes[30];
} CAN_Frame;

}
}

#endif // UDP_GLOABLS_H
