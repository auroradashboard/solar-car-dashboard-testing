#ifndef UDP_SENDER_H
#define UDP_SENDER_H

#include <boost/asio.hpp>
#include <boost/bind.hpp>

#include "globals.h"

namespace Solaris {
namespace UDP {

class Sender
{
public:
    Sender(const char* ip_address, const char* port);
    ~Sender();

    void send_can_frame(CAN_Frame *cf);

private:
    boost::asio::io_service _io_service;
    boost::asio::ip::udp::resolver _resolver;
    boost::asio::ip::udp::endpoint _endpoint;
    boost::asio::ip::udp::socket _socket;
};

}
}

#endif // UDP_SENDER_H
