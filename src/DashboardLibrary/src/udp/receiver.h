#ifndef UDP_RECEIVER_H
#define UDP_RECEIVER_H

#include <condition_variable>
#include <mutex>
#include <queue>
#include <atomic>

#include <boost/asio.hpp>
#include <boost/bind.hpp>

#include "globals.h"

namespace Solaris {
namespace UDP {

/****************************************************************************\
|  Data Types                                                                |
\****************************************************************************/

typedef boost::shared_ptr<UDP::CAN_Frame> can_frame_pointer;

class Receiver
{
public:
    Receiver(boost::asio::io_service& io_service,
             std::atomic<bool>* running,
             const char* s_listen_address,
             const char* s_multicast_address,
             const char* s_port);
    ~Receiver();

    void start_receive();
    void stop_receive();

    UDP::CAN_Frame* get_can_frame();

private:
    void _handle_receive(can_frame_pointer pointer,
                         const boost::system::error_code& error);

    UDP::CAN_Frame* _receive_can_frame();

    boost::asio::ip::udp::resolver _resolver;
    boost::asio::ip::udp::endpoint _endpoint;
    boost::asio::ip::udp::socket _socket;

    std::atomic<bool>* _running;

    std::queue<Solaris::UDP::CAN_Frame*> _can_frame_queue;
    std::condition_variable _cv_queue;
    std::mutex _cv_mutex;
};

}
}

#endif // UDP_RECEIVER_H
