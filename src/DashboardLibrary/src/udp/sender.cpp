#include "sender.h"

#include <iostream>

#include "globals.h"

using boost::asio::ip::udp;

/****************************************************************************\
|  Functions                                                                 |
\****************************************************************************/

Solaris::UDP::Sender::Sender(const char* ip_address, const char* port)
    : _io_service(),
      _resolver(_io_service),
      _socket(_io_service)
{
    udp::resolver::query query(udp::v4(), ip_address, port);
    _endpoint = *_resolver.resolve(query);

    _socket.open(_endpoint.protocol());
}

Solaris::UDP::Sender::~Sender()
{
    _socket.close();
    _resolver.cancel();
    _io_service.stop();
}

void Solaris::UDP::Sender::send_can_frame(CAN_Frame *cf)
{
    _socket.send_to(boost::asio::buffer(cf->bytes, sizeof(cf->bytes)), _endpoint);
}
