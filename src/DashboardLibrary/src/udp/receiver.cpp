#include "receiver.h"

#include <iostream>

#include "globals.h"

using boost::asio::ip::udp;
using namespace Solaris;

/****************************************************************************\
|  Functions                                                                 |
\****************************************************************************/

Solaris::UDP::Receiver::Receiver(boost::asio::io_service& io_service,
                                 std::atomic<bool>* running,
                                 const char* s_listen_address,
                                 const char* s_multicast_address,
                                 const char* s_port)
    : _resolver(io_service),
      _socket(io_service)
{
    udp::resolver::query query(udp::v4(), s_listen_address, s_port);
    _endpoint = *_resolver.resolve(query);

    _socket.open(_endpoint.protocol());
    _socket.set_option(udp::socket::reuse_address(true));
    _socket.bind(_endpoint);

    boost::asio::ip::address multicast_address;
    multicast_address = boost::asio::ip::address::from_string(s_multicast_address);

    _socket.set_option(boost::asio::ip::multicast::join_group(multicast_address));

    _running = running;
}

Solaris::UDP::Receiver::~Receiver()
{
    while(_can_frame_queue.size() > 0)
    {
        delete _can_frame_queue.front();
        _can_frame_queue.pop();
    }
}

void Solaris::UDP::Receiver::start_receive()
{
    can_frame_pointer pointer(new CAN_Frame());
    udp::endpoint remote_endpoint;

    _socket.async_receive_from(boost::asio::buffer(pointer->bytes, sizeof(pointer->bytes)),
                               remote_endpoint,
                               boost::bind(&Solaris::UDP::Receiver::_handle_receive, this,
                                           pointer,
                                           boost::asio::placeholders::error));
}

void Solaris::UDP::Receiver::stop_receive()
{
    _socket.cancel();
}

void Solaris::UDP::Receiver::_handle_receive(can_frame_pointer pointer,
                     const boost::system::error_code& error)
{
    UDP::CAN_Frame* cf = pointer.get();

    if(cf != nullptr)
    {
        std::lock_guard<std::mutex> lk(_cv_mutex);
        _can_frame_queue.push(cf);
    }

    _cv_queue.notify_all();

    if(_running->load())
    {
        start_receive();
    }
}

UDP::CAN_Frame* Solaris::UDP::Receiver::get_can_frame()
{
    std::unique_lock<std::mutex> lk(_cv_mutex);
    
    // Wait on condition variable until either
    //   a) There's a CAN Frame to return
    //   b) The timer timesout
    _cv_queue.wait_for(lk, std::chrono::seconds(1), [this] () { return _running->load(); } );

    if(!_can_frame_queue.empty())
    {
        // Return CAN Frame
        UDP::CAN_Frame* cf = _can_frame_queue.front();
        _can_frame_queue.pop();

        return cf;
    }
    else
    {
        // Timeout - return nullptr

        return nullptr;
    }
}

UDP::CAN_Frame* Solaris::UDP::Receiver::_receive_can_frame()
{
    UDP::CAN_Frame *cf = new CAN_Frame();

    udp::endpoint remote_endpoint;

    _socket.receive_from(boost::asio::buffer(cf->bytes, sizeof(cf->bytes)),
                         remote_endpoint);

    return cf;
}
