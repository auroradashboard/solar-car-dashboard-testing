#ifndef DB_DATABASE_H
#define DB_DATABASE_H

/******************************************************************************\
|  Includes                                                                  |
\******************************************************************************/

#include <sqlite3.h>
#include <boost/date_time.hpp>

#include "../buffer.h"

/******************************************************************************\
|  Data Types                                                                  |
\******************************************************************************/

namespace Solaris {
namespace DB {

class Database
{
public:
    Database(const char* path, Buffer::NetworkBuffer *nb);
    ~Database();

    void drop_tables();
    void create_tables();

    void check_buffers();
    void clear_buffers();

    void insert(Buffer::ChannelGroupValue *cgv);

    std::vector<Solaris::Buffer::ChannelValue*>* select(Solaris::CAN::Channel* c);
    std::vector<Solaris::Buffer::ChannelValue*>* select(Solaris::CAN::Channel* c, boost::posix_time::time_period tp);
private:
    sqlite3 *_db;
    Buffer::NetworkBuffer *_networkBuffer;
};

/******************************************************************************\
|  Functions                                                                   |
\******************************************************************************/

std::string create_where_clause(boost::posix_time::time_period tp);

}
}

#endif // DB_DATABASE_H
