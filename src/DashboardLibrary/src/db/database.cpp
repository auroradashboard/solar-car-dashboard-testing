#include "database.h"

#include <iostream>

#include "../buffer/channelgroupbuffer.h"
#include "../can/channelgroup.h"

using namespace Solaris::CAN;
using namespace Solaris::Buffer;

Solaris::DB::Database::Database(const char* path, NetworkBuffer *nb)
{
    int rc = sqlite3_open(path, &_db);

    if(rc)
    {
        std::cerr << "Unable to open database: " << path << std::endl;
        std::cerr << sqlite3_errmsg(_db) << std::endl;
        std::cerr << "Aborting" << std::endl;
        exit(0);
    }

    _networkBuffer = nb;
}

Solaris::DB::Database::~Database()
{
    int rc = sqlite3_close(_db);

    if(rc)
    {
        std::cerr << "Unable to close database " << std::endl;
        std::cerr << sqlite3_errmsg(_db) << std::endl;
        std::cerr << "Aborting" << std::endl;
        exit(0);
    }
}

void Solaris::DB::Database::drop_tables()
{
    std::string select_query = "SELECT 'DROP TABLE \"' || name || '\";' FROM sqlite_master WHERE type = 'table';";
    std::string drop_query = "";
    sqlite3_stmt *select_stmt;

    sqlite3_prepare_v2(_db, select_query.data(), select_query.size(), &select_stmt, nullptr);

    // This loop gets all the DROP TABLE statements created by the previous query
    // and concatentates them into a single std::string

    bool finished = false;
    // TODO Look at rearranging this. eg while((s = sqlite3_step(select_stmt) != SQLITE_DONE)
    while(!finished)
    {
        int s = sqlite3_step(select_stmt);
        if(s == SQLITE_ROW)
        {
            const char* drop_text = reinterpret_cast<const char*>(sqlite3_column_text(select_stmt, 0));
            drop_query.append(drop_text);
            drop_query.append("\n");
        }
        else if(s == SQLITE_DONE)
        {
            finished = true;
        }
        else
        {
            // Something weird has happened
            std::cerr << "Error while dropping tables";
            exit(0);
        }
    }

    sqlite3_finalize(select_stmt);

    // Execute the concatenated DROP TABLE statements


    char *errMsg;
    int rc = sqlite3_exec(_db, drop_query.data(), nullptr, 0, &errMsg);
    if(rc)
    {
        std::cerr << "Error dropping tables: " << std::endl;
        std::cerr << errMsg << std::endl;
        std::cerr << std::endl << drop_query << std::endl;
        std::cerr << "Aborting" << std::endl;

        sqlite3_free(errMsg);

        exit(0);
    }
}

void Solaris::DB::Database::create_tables()
{
    for(auto &cgb: _networkBuffer->_buffers)
    {
        ChannelGroup *cg = cgb->_channelGroup;
        std::string query = cg->getCreateStatement();

        char *errMsg = 0;

        int rc = sqlite3_exec(_db, query.data(), nullptr, 0, &errMsg);
        if(rc)
        {
            std::cerr << "Error creating table: " << cg->sqlName() << std::endl;
            std::cerr << errMsg << std::endl;
            std::cerr << std::endl << query << std::endl;
            std::cerr << "Aborting" << std::endl;

            sqlite3_free(errMsg);

            exit(0);
        }
    }
}

void Solaris::DB::Database::insert(ChannelGroupValue *cgv)
{
    std::string query = cgv->get_sql_insert_statement();

    char *errMsg = 0;

    int rc = sqlite3_exec(_db, query.data(), nullptr, 0, &errMsg);
    if(rc)
    {
        std::cerr << "Error executing INSERT query: " << std::endl;
        std::cerr << std::endl << query << std::endl;
        std::cerr << std::endl << errMsg << std::endl;
        std::cerr << std::endl << query << std::endl;

        sqlite3_free(errMsg);
    }
}

std::vector<Solaris::Buffer::ChannelValue*>* Solaris::DB::Database::select(Channel* c)
{
    boost::posix_time::ptime now = boost::posix_time::microsec_clock::universal_time();
    boost::posix_time::time_period tp = boost::posix_time::time_period(now, now);
    return select(c, tp);
}

std::vector<Solaris::Buffer::ChannelValue*>* Solaris::DB::Database::select(Channel* c, boost::posix_time::time_period tp)
{
    std::vector<Solaris::Buffer::ChannelValue*>* values = new std::vector<Solaris::Buffer::ChannelValue*>;

    std::string select = c->get_select_statement();
    std::string where = create_where_clause(tp);

    std::string query = select + " " + where + ";";

    char *errMsg = 0;

    sqlite3_stmt* stmt;
    int rc;

    rc = sqlite3_prepare_v2(_db, query.data(), -1, &stmt, 0);
    if(rc != SQLITE_OK)
    {
        std::cerr << "Error preparing SELECT query: " << std::endl;
        std::cerr << std::endl << query << std::endl;
        std::cerr << std::endl << errMsg << std::endl;
        std::cerr << std::endl << query << std::endl;
        std::cerr << "Aborting" << std::endl;

        sqlite3_free(errMsg);

        exit(0);
    }

    // Iterate over rows

    rc = sqlite3_step(stmt);

    while(rc == SQLITE_ROW)
    {
        Solaris::Buffer::ChannelValue* cv;

        switch(c->field->type)
        {
        case F_UINT8:
        case F_UINT16:
        case F_UINT32:
            cv = new Solaris::Buffer::ChannelValue(Solaris::Buffer::CVT_UINT);

            cv->store_uint(sqlite3_column_int(stmt, 0));
            break;

        case F_INT8:
        case F_INT16:
        case F_INT32:
            cv = new Solaris::Buffer::ChannelValue(Solaris::Buffer::CVT_INT);

            cv->store_int(sqlite3_column_int(stmt, 0));
            break;

        case F_FLOAT32:
        case F_FLOAT64:
            cv = new Solaris::Buffer::ChannelValue(Solaris::Buffer::CVT_FLOAT);

            cv->store_float(sqlite3_column_double(stmt, 0));

            break;

        case F_BOOL:
            cv = new Solaris::Buffer::ChannelValue(Solaris::Buffer::CVT_INT);

            cv->store_bool((bool) sqlite3_column_int(stmt, 0));

            break;

        case F_NO_TYPE:
            // FIXME Should never happen
            continue;
        }
        values->push_back(cv);

        rc = sqlite3_step(stmt);
    }


    // Cleanup

    sqlite3_finalize(stmt);

    return values;
}

void Solaris::DB::Database::check_buffers()
{
    for(auto &cgb: _networkBuffer->_buffers)
    {
        if(cgb->is_full())
        {
            while(!cgb->is_empty())
            {
                ChannelGroupValue *cgv = cgb->pop();
                insert(cgv);
                delete cgv;
            }
        }
    }
}

void Solaris::DB::Database::clear_buffers()
{
    for(auto &cgb: _networkBuffer->_buffers)
    {
        while(!cgb->is_empty())
        {
            ChannelGroupValue *cgv = cgb->pop();
            insert(cgv);
            delete cgv;
        }
    }
}

std::string Solaris::DB::create_where_clause(boost::posix_time::time_period tp)
{
    // TODO Make this ptime --> SQLite friendly string a function
    std::string start = to_iso_extended_string(tp.begin());
    std::replace(start.begin(), start.end(), ',', '.');
    std::replace(start.begin(), start.end(), 'T', ' ');
    start.insert(0, "'");
    start.append("'");

    std::string end = to_iso_extended_string(tp.end());
    std::replace(end.begin(), end.end(), ',', '.');
    std::replace(end.begin(), end.end(), 'T', ' ');
    end.insert(0, "'");
    end.append("'");

    std::string clause = " WHERE timestamp BETWEEN datetime(" + start + ") AND datetime(" + end + ")";

    return clause;
}
