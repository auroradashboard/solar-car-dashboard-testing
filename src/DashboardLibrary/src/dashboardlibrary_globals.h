#ifndef DASHBOARDLIBRARY_GLOBALS_H
#define DASHBOARDLIBRARY_GLOBALS_H

#include <iostream>

namespace Solaris {
namespace CAN {

/****************************************************************************\
|  Constants                                                                 |
\****************************************************************************/

#define CONFIG_DIR              ("..res/config")
#define DEVICE_CONFIG_DIR       ("../res/config/devices/")
#define CAR_CONFIG_FILE         ("../res/config/car.json")

#define CONFIG_FILE_EXTENSION   (".json")

/****************************************************************************\
|  Data Types                                                                |
\****************************************************************************/



/****************************************************************************\
|  Functions                                                                 |
\****************************************************************************/


}
}

#endif // DASHBOARDLIBRARY_GLOBALS_H
