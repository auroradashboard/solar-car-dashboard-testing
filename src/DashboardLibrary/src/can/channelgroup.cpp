#include "channelgroup.h"

#include <algorithm>

Solaris::CAN::ChannelGroup::ChannelGroup(DeviceInstance *di, Frame *f)
{
    deviceInstance = di;
    frame = f;

    for(auto &field_iter: frame->fields) {
        if(field_iter->type != F_NO_TYPE) {
            // Create Channel and ChannelBuffer
            Channel *c = new Channel(field_iter, this);

            channels.push_back(c);
        }
        else {
            // No type
            // Don't create a channel for it
        }
    }
}

Solaris::CAN::ChannelGroup::~ChannelGroup() {
    for(auto &c: channels) {
        delete c;
    }
}

std::string Solaris::CAN::ChannelGroup::sqlName() {
    std::string name = deviceInstance->name + "__" + frame->name;
    std::replace(name.begin(), name.end(), ' ', '_');
    return name;
}

std::string Solaris::CAN::ChannelGroup::getCreateStatement() {
    std::string query = "CREATE TABLE \"" + sqlName() + "\" (\n";
    query += "    timestamp TEXT primary key";

    // Create a column for each Channel
    for(auto &c: channels) {
        query += ",\n    \"" + c->sqlName() + "\" " + c->sqlType();
    }

    query += ");";

    return query;
}

std::string Solaris::CAN::ChannelGroup::getInsertStatement() {
    std::string query = "INSERT INTO " + sqlName() + " (timestamp";

    for(auto &c: channels) {
        query += ", " + c->sqlName();
    }

    query += ") VALUES (?"; // For timestamp

    // TODO Escape inputs going into SQL. May currently be vulnerable
    for(unsigned int i = 0; i < channels.size(); i++) {
        query += ", ?";
    }

    query += ");";

    return query;
}

Solaris::CAN::Channel* Solaris::CAN::ChannelGroup::get_channel_by_name(std::string str)
{
    for(auto &c: channels)
    {
        if(str.compare(c->field->name) == 0)
        {
            return c;
        }
    }
    return nullptr;
}
