#ifndef CAN_FIELD_H
#define CAN_FIELD_H

#include <string>

#include <rapidjson/document.h>

#include "../dashboardlibrary_globals.h"

namespace Solaris {
namespace CAN {

/******************************************************************************\
|  Forward Declarations                                                        |
\******************************************************************************/

class Frame;

/******************************************************************************\
|  Data Types                                                                  |
\******************************************************************************/

/**
 * @brief TODO
 */
typedef enum Field_Type_Enum {
    F_NO_TYPE = 0,
    F_UINT8,
    F_UINT16,
    F_UINT32,
    F_INT8,
    F_INT16,
    F_INT32,
    F_FLOAT32,
    F_FLOAT64,
    F_BOOL
} Field_Type_Enum;

/**
 * @brief The Field class represents a piece of data stored within a Frame.
 */
class Field
{
public:
    /**
     * @brief Field Creates a Field for a given Frame from a JSON config
     * @param parent The Frame this Field is part of
     * @param json_field A JSON object containing the configuration of this Field.
     */
    Field(Frame* parent, const rapidjson::Value& json_field);

    /**
     * @brief name The human-readable name for this field
     */
    std::string name;

    /**
     * @brief frame The Frame this Field is part of
     */
    Frame* frame;

    /**
     * @brief startBit The index of the first bit of this Field
     */
    unsigned int startBit;

    /**
     * @brief width The number of bits this Field takes up
     */
    unsigned int width;

    /**
     * @brief type The data type stored in this Field
     */
    Field_Type_Enum type;

    /**
     * @brief unit The unit of whatever data is stored in this Field
     */
    std::string unit;
};

/******************************************************************************\
|  Data Types                                                                  |
\******************************************************************************/

/**
 * @brief StringToTypeEnum TODO
 * @param str
 * @return
 */
CAN::Field_Type_Enum StringToFieldTypeEnum(std::string str);

}
}

#endif // CAN_FIELD_H
