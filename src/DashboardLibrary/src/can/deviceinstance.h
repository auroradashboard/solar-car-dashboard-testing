#ifndef CAN_DEVICEINSTANCE_H
#define CAN_DEVICEINSTANCE_H

#include <string>
#include <vector>

#include "device.h"

namespace Solaris {
namespace CAN {

/******************************************************************************\
|  Forward Declarations                                                        |
\******************************************************************************/

class Channel;
class ChannelGroup;

/******************************************************************************\
|  Data Types                                                                  |
\******************************************************************************/

/**
 * @brief The DeviceInstance class represents a physical instance of a Device type on
 * a CAN network.
 *
 * Many DeviceInstances can point to the same Device object.
 */
class DeviceInstance
{
public:
    /**
     * @brief DeviceInstance Creates a DeviceInstance object with the given values.
     *
     * @param name The name given to this instance
     * @param device A pointer to the Device to the device this is an instance of
     * @param baseAddress The base address this instance uses when sending messages
     */
    DeviceInstance(std::string name, Device* device, int baseAddress);

    /**
      * @brief Destroys the DeviceInstance and it's ChannelGroups and ChannelBufferGroups
      */
    ~DeviceInstance();

    Channel* get_channel_by_name(std::string str);

    ChannelGroup* get_channel_group_by_name(std::string str);

    /**
     * @brief The name given to this instance. Should be unique.
     */
    std::string name;

    /**
     * @brief A pointer to the Device that this instance is based on.
     */
    Device* device;

    /**
     * @brief The lowest message ID that this instance will transmit.
     */
    unsigned int baseAddress;

    /**
     * @brief The ChannelGroups associated with this DeviceInstance.
     *
     * There should be one ChannelGroup for each Frame in device
     */
    std::vector<ChannelGroup*> channelGroups;
};

}
}

#endif // CAN_DEVICEINSTANCE_H
