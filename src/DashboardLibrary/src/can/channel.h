#ifndef CAN_CHANNEL_H
#define CAN_CHANNEL_H

#include <string>

#include "field.h"

namespace Solaris {
namespace CAN {

class ChannelGroup;

/**
 * @brief The Channel class represents a Field from a specific DeviceInstance
 *
 * For example, if there are two DeviceInstances using the same Device, and
 * this Device has one Frame with one Field, two Channels will used to represent
 * this Field - one for each DeviceInstance.
 *
 * The Channel is not directly associate with a DeviceInstance. Instead it is
 * associated with a ChannelGroup through which it can access the relevant Frame.
 *
 * The Channel can access its Frame through the Field or ChannelGroup.
 */
class Channel
{
public:

    /**
     * @brief Creates a Channel for the given Field f as part of the ChannelGroup cg.
     * @param f The field this Channel represents
     * @param cg The ChannelGroup this Channel belongs to. Used to gain access to the DeviceInstance
     */
    Channel(CAN::Field *f, CAN::ChannelGroup *cg);

    std::string sqlName();

    std::string sqlType();

    std::string get_select_statement();

    /**
     * @brief field The Field this Channnel gets its data from
     */
    CAN::Field *field;

    /**
     * @brief channelGroup The ChannelGroup this Channel is part of
     */
    ChannelGroup *channelGroup;
};

}
}

#endif // CAN_CHANNEL_H
