#ifndef CAN_DEVICE_H
#define CAN_DEVICE_H

#include <vector>

#include "frame.h"

namespace Solaris {
namespace CAN {

/**
 * @brief The Device class represents a type of device which may be on the CAN network.
 *
 * This class is an abstract device only. A specific physical device should be represented
 * using a DeviceInstance pointing to a Device.
 */
class Device
{
public:
    /**
     * @brief Device Loads a device from a JSON configuration file.
     * @param path The path to the configuration file.
     */
    Device(const char* path);

    /**
     * @brief Destroys the Device and its Frames
     */
    ~Device();

    /**
     * @brief The human-readable name of the Device
     */
    std::string name;

    /**
     * @brief The Frames this Devices can transmit.
     */
    std::vector<Frame*> frames;
};

}
}

#endif // CAN_DEVICE_H
