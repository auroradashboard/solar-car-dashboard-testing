#ifndef CHANNELVALUE_H
#define CHANNELVALUE_H

#include <typeinfo>

#include "field.h"

namespace Solaris {
namespace CAN {

/******************************************************************************\
|  Data Types                                                                  |
\******************************************************************************/

/**
 * @brief TODO
 */

typedef enum Channel_Value_Type_Enum {
    CVT_BOOL,
    CVT_FLOAT,
    CVT_INT,
    CVT_UINT
} Channel_Value_Type_Enum;

class ChannelValue
{
public:
    ChannelValue(CAN::Channel_Value_Type_Enum t);

    void store_bool(bool d);
    void store_float(float d);
    void store_int(int d);
    void store_uint(unsigned int d);

    bool get_bool();
    float get_float();
    int get_int();
    unsigned int get_uint();

    std::string get_sql_value();

    CAN::Channel_Value_Type_Enum get_type();
private:
    CAN::Channel_Value_Type_Enum type;

    union
    {
        bool bool_data;
        float float_data;
        int int_data;
        unsigned int uint_data;
    };
};

/******************************************************************************\
|  Functions                                                                   |
\******************************************************************************/

CAN::Channel_Value_Type_Enum field_to_channel_value_type(CAN::Field_Type_Enum ft);

}
}

#endif // CHANNELVALUE_H
