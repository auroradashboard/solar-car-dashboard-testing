#include "network.h"

#include <algorithm>
#include <cmath>
#include <fstream>
#include <iostream>
#include <map>
#include <streambuf>
#include <string>
#include <vector>

#include <rapidjson/document.h>

#include "../dashboardlibrary_globals.h"

#include "channelgroup.h"

using namespace std;
using namespace rapidjson;

Solaris::CAN::Network::Network(const char* path)
{
    loadDeviceInstances(path);
}

Solaris::CAN::Network::~Network()
{
    for(auto &di: deviceInstances) {
        delete di;
    }

    for(auto &p: loadedDevices) {
        delete p.second;
    }
}

void Solaris::CAN::Network::loadDeviceInstances(const char* path) {
    ifstream ifs(path);
    string file_contents((istreambuf_iterator<char>(ifs)), istreambuf_iterator<char>());

    if(file_contents.size() <= 0) {
        // Invalid config file
        cerr << "Coud not load car config file: " << path << endl;
        cerr << "Aborting." << endl;

        exit(0);
    }

    Document network;
    network.Parse(file_contents.data());

    if(network.HasMember("deviceInstances")) {
        const Value& json_deviceInstances = network["deviceInstances"];

        deviceInstances = vector<DeviceInstance*>();
        loadedDevices = map<string,Device*>();

        if(json_deviceInstances.IsArray()) {
            for(SizeType i = 0; i < json_deviceInstances.Size(); i++) {
                const Value& json_deviceInstance = json_deviceInstances[i];

                string name = "";
                Device *device = nullptr;
                int baseAddress = -1;

                if(json_deviceInstance.HasMember("name")) {
                    name = json_deviceInstance["name"].GetString();
                }

                if(json_deviceInstance.HasMember("baseAddress")) {
                    string num_string = json_deviceInstance["baseAddress"].GetString();
                    baseAddress = strtol(num_string.data(), nullptr, 16);
                }

                if(json_deviceInstance.HasMember("device")) {
                    string device_string = json_deviceInstance["device"].GetString();

                    auto device_pair = loadedDevices.find(device_string);
                    if(device_pair != loadedDevices.end()) {
                        //
                        // Device already loaded
                        //

                        device = device_pair->second;
                    } else {
                        // Device not yet loaded

                        //
                        // Generate filename for device config file
                        //

                        string device_string_prepped = device_string;

                        // Replace spaces with underscores
                        for(unsigned int j = 0; j < device_string_prepped.size(); j++) {
                            if(device_string_prepped[j] == ' ') {
                                device_string_prepped[j] = '_';
                            }
                        }

                        // Add path and file extension
                        string dev_path = DEVICE_CONFIG_DIR + device_string_prepped + CONFIG_FILE_EXTENSION;

                        //
                        // Create new device
                        //

                        device = new Device(dev_path.data());

                        //
                        // Add it to loadedDevices
                        //

                        loadedDevices.insert(pair<string, Device*>(device_string, device));
                    }
                }
                deviceInstances.push_back(new DeviceInstance(name, device, baseAddress));
            }
        }
    } else {
        // Invalid config file
        cerr << "Invalid car config file: " << path << endl;
        cerr << "Aborting." << endl;

        exit(0);
    }
}

Solaris::CAN::ChannelGroup* Solaris::CAN::Network::getChannelGroup(unsigned int id)
{
    for(auto &di: deviceInstances)
    {
        for(auto &cg: di->channelGroups)
        {
            if(id == (di->baseAddress + cg->frame->id))
            {
                return cg;
            }
        }
    }
    // No match
    return nullptr;
}

Solaris::CAN::Channel* Solaris::CAN::Network::get_channel_by_name(std::string str)
{
    std::string di_name = str.substr(0, str.find("."));
    std::string cg_str = str.substr(str.find(".")+1, std::string::npos);

    CAN::DeviceInstance* di = get_device_instance_by_name(di_name);
    return di->get_channel_by_name(cg_str);
}

Solaris::CAN::DeviceInstance* Solaris::CAN::Network::get_device_instance_by_name(std::string str)
{
    for(auto &di: deviceInstances)
    {
        if(str.compare(di->name) == 0)
        {
            return di;
        }
    }
    return nullptr;
}
