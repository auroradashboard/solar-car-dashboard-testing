#include "deviceinstance.h"

#include "channel.h"
#include "channelgroup.h"
#include "field.h"

using namespace std;

Solaris::CAN::DeviceInstance::DeviceInstance(string name, Device* device, int baseAddress)
{
    this->name = name;
    this->device = device;
    this->baseAddress = baseAddress;

    // Iterate over frames in device
    for(auto &frame_iter: device->frames) {
        // Create ChannelGroup and ChannelBufferGroup
        ChannelGroup *cg = new ChannelGroup(this, frame_iter);

        channelGroups.push_back(cg);
    }
}


Solaris::CAN::DeviceInstance::~DeviceInstance() {
    for(auto &cg: channelGroups) {
        delete cg;
    }
}

Solaris::CAN::Channel* Solaris::CAN::DeviceInstance::get_channel_by_name(std::string str)
{
    std::string cg_name = str.substr(0, str.find("."));
    std::string c_str = str.substr(str.find(".")+1, std::string::npos);

    CAN::ChannelGroup* cg = get_channel_group_by_name(cg_name);
    if(cg)
    {
        return cg->get_channel_by_name(c_str);
    }
    else
    {
        return nullptr;
    }
}

Solaris::CAN::ChannelGroup* Solaris::CAN::DeviceInstance::get_channel_group_by_name(std::string str)
{
    for(auto &cg: channelGroups)
    {
        if(str.compare(cg->frame->name) == 0)
        {
            return cg;
        }
    }
    return nullptr;
}
