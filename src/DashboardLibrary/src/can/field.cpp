#include "field.h"

#include "../dashboardlibrary_globals.h"

Solaris::CAN::Field::Field(Frame* parent, const rapidjson::Value& json_field) {
    frame = parent;

    if(json_field.HasMember("name")) {
        name = json_field["name"].GetString();
    }

    if(json_field.HasMember("startBit")) {
        startBit = json_field["startBit"].GetUint();
    }

    if(json_field.HasMember("width")) {
        width = json_field["width"].GetUint();
    }

    if(json_field.HasMember("type")) {
        type = StringToFieldTypeEnum(json_field["type"].GetString());
    }

    if(json_field.HasMember("unit")) {
        unit = json_field["unit"].GetString();
    }
}

Solaris::CAN::Field_Type_Enum Solaris::CAN::StringToFieldTypeEnum(std::string str) {
    if(str.compare("uint8") == 0) {
        return Solaris::CAN::F_UINT8;
    }
    if(str.compare("uint16") == 0) {
        return Solaris::CAN::F_UINT16;
    }
    if(str.compare("uint32") == 0) {
        return Solaris::CAN::F_UINT32;
    }

    if(str.compare("int8") == 0) {
        return Solaris::CAN::F_INT8;
    }
    if(str.compare("int16") == 0) {
        return Solaris::CAN::F_INT16;
    }
    if(str.compare("int32") == 0) {
        return Solaris::CAN::F_INT32;
    }

    if(str.compare("float32") == 0) {
        return Solaris::CAN::F_FLOAT32;
    }
    if(str.compare("float64") == 0) {
        return Solaris::CAN::F_FLOAT64;
    }

    if(str.compare("bool") == 0) {
        return Solaris::CAN::F_BOOL;
    }

    return Solaris::CAN::F_NO_TYPE;
}
