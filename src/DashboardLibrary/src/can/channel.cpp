#include "channel.h"

#include <algorithm>

#include "channelgroup.h"

Solaris::CAN::Channel::Channel(Field *f, ChannelGroup *cg)
{
    field = f;
    channelGroup = cg;
}


std::string Solaris::CAN::Channel::sqlName() {
    std::string out_name = field->name;

    std::replace(out_name.begin(), out_name.end(), '-', '_');
    std::replace(out_name.begin(), out_name.end(), ' ', '_');

    return out_name;
}

std::string Solaris::CAN::Channel::sqlType() {
    switch(field->type) {
    case Solaris::CAN::F_UINT8:
        return "INTEGER";
    case Solaris::CAN::F_UINT16:
        return "INTEGER";
    case Solaris::CAN::F_UINT32:
        return "INTEGER";
    case Solaris::CAN::F_INT8:
        return "INTEGER";
    case Solaris::CAN::F_INT16:
        return "INTEGER";
    case Solaris::CAN::F_INT32:
        return "INTEGER";
    case Solaris::CAN::F_FLOAT32:
        return "REAL";
    case Solaris::CAN::F_FLOAT64:
        return "REAL";
    case Solaris::CAN::F_BOOL:
        return "INTEGER";
    case Solaris::CAN::F_NO_TYPE:
        return "";
    }
    // TODO Add error handling
    // Should never get here
    return "";
}

std::string Solaris::CAN::Channel::get_select_statement()
{
    std::string table_name = channelGroup->sqlName();
    std::string column_name = sqlName();

    std::string query = "";
    query.append("SELECT ");
    query.append(column_name);
    query.append(" FROM ");
    query.append(table_name);

    return query;
}
