#ifndef CAN_NETWORK_H
#define CAN_NETWORK_H

#include <map>
#include <string>
#include <vector>

#include <boost/date_time.hpp>

#include "deviceinstance.h"

namespace Solaris {
namespace CAN {

/**
 * @brief The Network class represents a CAN network.
 *
 * It is used to manage a collection of DeviceInstances, ChannelGroups, etc.
 */
class Network
{
public:
    /**
     * @brief Creates a Network and calls loadFile with the given path.
     * @param path Path to the config file to load
     */
    Network(const char* path);

    /**
     * @brief Destroys the network and all DeviceInstances and Devices it created.
     */
    ~Network();

    /**
     * @brief loadDeviceInstances loads the provided JSON config file and creates DeviceInstances based on it
     * @param path Path to the config file
     * @return
     */
    void loadDeviceInstances(const char* path);

    CAN::ChannelGroup* getChannelGroup(unsigned int id);

    CAN::Channel* get_channel_by_name(std::string str);

    CAN::DeviceInstance* get_device_instance_by_name(std::string str);

    /**
     * @brief The DeviceInstances that make up this network
     */
    std::vector<DeviceInstance*> deviceInstances;

    /**
     * @brief loadedDevices stores all the Devices that are created, along with their name
     *
     * The name is used in network config files to refer to the device.
     */
    std::map<std::string, Device*> loadedDevices;
};

}
}

#endif // CAN_NETWORK_H
