#ifndef CAN_FRAME_H
#define CAN_FRAME_H

#include <string>
#include <vector>

#include <rapidjson/document.h>

#include "field.h"

namespace Solaris {
namespace CAN {

class Device;

/**
 * @brief The Frame class represents a CAN frame.
 *
 * A frame is transmitted from a specific Device and can contain many Fields.
 *
 * A frame can either know it's absolute or relative message ID. If relative, the
 * ID is added to the DeviceInstance's base address.
 */
class Frame
{
public:
    /**
     * @brief Frame Creates a Frame belonging to the given Device based on the provided JSON
     * @param parent The Device the Frame belongs to
     * @param json_frame JSON containing the configuration of the Frame
     */
    Frame(Device* parent, const rapidjson::Value& json_frame);

    /**
     * @brief Destroys the Frame and all its Fields.
     */
    ~Frame();

    /**
     * @brief The human-readable name of the Frame (optional)
     */
    std::string name;

    /**
     * @brief The Device this Frame is transmitted from
     */
    Device *device;

    /**
     * @brief The ID this frame is sent with
     *
     * If offset is false, id stores the absolute message ID of the Frame.
     * If offset is true, id stores the offset to be added to the DeviceInstance's base address.
     */
    unsigned int id;

    /**
     * @brief Whether is an absolute address or an offset
     */
    bool offset; // Stores if the id is an offset from the device's base address. If not, it's a fixed ID

    /**
     * @brief The frequency at which this frame can be expected (in Hz)
     */
    int frequency;

    /**
     * @brief The Fields that are contained within this Frame.
     */
    std::vector<Field*> fields;
};

}
}

#endif // CAN_FRAME_H
