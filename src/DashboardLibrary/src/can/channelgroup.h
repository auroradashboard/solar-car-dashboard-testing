#ifndef CAN_CHANNELGROUP_H
#define CAN_CHANNELGROUP_H

#include <string>
#include <vector>

#include "channel.h"
#include "deviceinstance.h"
#include "frame.h"

namespace Solaris {
namespace CAN {

/**
 * @brief The ChannelGroup class is a collection of Channels that are related.
 *
 * Most commonly, a ChannelGroup is used to store a Channel for each Field in a Frame.
 */
class ChannelGroup
{
public:
    /**
     * @brief ChannelGroup Creates a ChannelGroup and Channels for the given DeviceInstance and Frame.
     */
    ChannelGroup(DeviceInstance *di, Frame *f);

    /**
     * @brief Destroys the ChannelGroup and its Channels
     */
    ~ChannelGroup();

    std::string sqlName();

    std::string getCreateStatement();
    std::string getInsertStatement();


    Channel* get_channel_by_name(std::string str);

    /**
     * @brief The Frame this ChannelGroup stores Channels for
     */
    Frame *frame;

    /**
     * @brief The DeviceInstance this ChannelGroup stores Channels for
     */
    DeviceInstance *deviceInstance;

    /**
     * @brief The Channels this ChannelGroup manages
     */
    std::vector<Channel*> channels;
};

}
}

#endif // CAN_CHANNELGROUP_H
