#include "device.h"

#include <fstream>
#include <iostream>
#include <streambuf>
#include <string>

#include <rapidjson/document.h>

using namespace std;
using namespace rapidjson;

Solaris::CAN::Device::Device(const char* path) {
    ifstream ifs(path);
    string file_contents((istreambuf_iterator<char>(ifs)), istreambuf_iterator<char>());

    if(file_contents.size() == 0) {
        cerr << "Error loading device config: " << path << endl;
        cerr << "File is empty or does not exist";
    }

    Document device;
    device.Parse(file_contents.data());

    if(device.HasMember("device")) {
        name = device["device"].GetString();
    }

    if(device.HasMember("frames")) {
        const Value& json_frames = device["frames"];

        if(json_frames.IsArray()) {
            for(SizeType i = 0; i < json_frames.Size(); i++) {
                const Value& json_frame = json_frames[i];

                Frame *frame = new Frame(this, json_frame);
                frames.push_back(frame);
            }
        }
    }
}

Solaris::CAN::Device::~Device() {
    for(auto &f: frames) {
        delete f;
    }
}
