#include "frame.h"

using namespace rapidjson;

Solaris::CAN::Frame::Frame(Device* parent, const rapidjson::Value& json_frame)
{
    device = parent;

    if(json_frame.HasMember("name")) {
        name = json_frame["name"].GetString();
    }

    if(json_frame.HasMember("idOffset")) {
        id = json_frame["idOffset"].GetUint();
        offset = true;
    }

    if(json_frame.HasMember("id")) {
        id = json_frame["id"].GetUint();
        offset = false;
    }

    if(json_frame.HasMember("frequency")) {
        frequency = json_frame["frequency"].GetUint();
    }

    if(json_frame.HasMember("fields")) {
        const Value& json_fields = json_frame["fields"];

        if(json_fields.IsArray()) {
            for(SizeType i = 0; i < json_fields.Size(); i++) {
                const Value& json_field = json_fields[i];

                Solaris::CAN::Field *field = new Solaris::CAN::Field(this, json_field);
                fields.push_back(field);
            }
        }
    }
}

Solaris::CAN::Frame::~Frame() {
    for(auto &f: fields) {
        delete f;
    }
}
