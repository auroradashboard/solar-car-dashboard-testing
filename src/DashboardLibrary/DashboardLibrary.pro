################################################################################
#  BUILD DETAILS
################################################################################

TEMPLATE = lib

CONFIG += c++11
CONFIG += warn_on
CONFIG += debug_and_release

CONFIG -= app_bundle
CONFIG -= qt

debug:QMAKE_CXX_FLAGS += -O0

unix:target.path = /usr/local/lib
unix:INSTALLS += target

################################################################################
#  FILES
################################################################################

SOURCES += \
    src/dashboardlibrary_globals.cpp \
    \
    src/can/channel.cpp \
    src/can/channelGroup.cpp \
    src/can/device.cpp \
    src/can/deviceinstance.cpp \
    src/can/field.cpp \
    src/can/frame.cpp \
    src/can/network.cpp \
    \
    src/udp/receiver.cpp \
    src/udp/sender.cpp \
    \
    src/db/database.cpp \
    \
    src/buffer/networkbuffer.cpp \
    src/buffer/channelgroupvalue.cpp \
    src/buffer/channelvalue.cpp \
    src/buffer/channelgroupbuffer.cpp \
    \
    src/tcp/request.cpp \
    src/tcp/server.cpp \
    src/tcp/response.cpp \
    src/tcp/client.cpp \
    src/tcp/streamserver.cpp \
    src/tcp/streamclient.cpp

HEADERS += \
    src/dashboardlibrary.h \
    src/dashboardlibrary_globals.h \
    src/buffer.h \
    src/can.h \
    src/db.h \
    src/udp.h \
    src/tcp.h \
    \
    src/can/channel.h \
    src/can/channelGroup.h \
    src/can/device.h \
    src/can/deviceinstance.h \
    src/can/field.h \
    src/can/frame.h \
    src/can/network.h \
    \
    src/udp/globals.h \
    src/udp/receiver.h \
    src/udp/sender.h \
    \
    src/db/database.h \
    \
    src/buffer/networkbuffer.h \
    src/buffer/channelgroupvalue.h \
    src/buffer/channelvalue.h \
    src/buffer/channelgroupbuffer.h \
    src/tcp/request.h \
    src/tcp/server.h \
    src/tcp/response.h \
    src/tcp/client.h \
    src/tcp/streamserver.h \
    src/tcp/streamclient.h

TARGET = DashboardLibrary

################################################################################
#  OUTPUT DIRECTORIES
################################################################################

DESTDIR = ../../bin/
OBJECTS_DIR = ../../build/

Debug:DESTDIR = ../../bin/debug/
Debug:OBJECTS_DIR = ../../build/debug/

Release:DESTDIR = ../../bin/release/
Release:OBJECTS_DIR = ../../build/release/

# These directories are used to hold 3rd party libraries
# In Linux these should be installed in the system instead
# They should not be under version control
win32:LIBS += -L$$PWD/../../lib
win32:INCLUDEPATH += $$PWD/../../include

################################################################################
#  LIBRARIES
################################################################################

LIBS += -lsqlite3

win32:LIBS += -lboost_system-mgw48-mt-1_58
win32:LIBS += -lboost_date_time-mgw48-mt-1_58
win32:LIBS += -lws2_32
win32:LIBS += -lmswsock

unix:LIBS += -lpthread
