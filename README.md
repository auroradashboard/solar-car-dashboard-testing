# Installation #

Using the Qt installer install the following:

 * Qt >> Qt 5.5 >> MinGW 4.9.2 32 bit
 * Qt >> Qt 5.5 >> Source Components >> Add-ons >> qtquickcontrols
 * Qt >> Qt 5.5 >> Source Components >> Add-ons >> qtquick1
 * Qt >> Qt 5.5 >> Qt Quick 1
 * Qt >> Qt 5.5 >> Qt Quick Controls
 * Tools >> Qt Creator 3.5.0
 * Tools >> MinGW 4.9.2

# Project Setup #

To open the project in QT Creator open the file

> ./src/SolarisDashboard.pro

From here QT Creator will ask you to set up the build configuration. The default configuration will most likely be named "Desktop Qt 5.5.0 MinGW 32bit" or similar.

Click on the "Details" button to expand it and change the Debug and Release paths so they point to the directory the project file is in.

> C:/path/to/project/directory/GitRepoIsHere/src/

This will ensure that the files are built into ./build/ and the  final output is all put into the ./bin/ directory. Click the "Configue Project" button below the section you just filled out to generate the SolarisDashboard.pro.user file

Next, click on the "Projects" tab on the left menu. Select "Run" ("Build" should currently be selected) from the dark box near the top of the screen.

Under the "Run" heading, setting the Working Directory to the bin/ directory (in the same place as the ./src/ and ./res/ folders from the git repository). This will ensure that the executable can find the configuration files in ./res/

# Dependencies #

This project depends on the following libraries:
* Boost
* rapidjson

Both can be installed either in your system, or specifically for the project by putting them in ./include and ./lib.
